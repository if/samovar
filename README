samovar-alpha-0.1
License: GPLv3


---------- OVERVIEW

'Samovar' is a roguelike game, currently in its alpha version.  It's
heavily inspired by Brogue, and to a lesser extent by Dungeon Crawl
Stone Soup.  Like most other roguelike games, it has potions,
monsters, and scrolls, and levels are randomly generated.
Thematically, the game is centered around drinking tea and fighting
semi-sentient mushrooms.

Here's a quick summary of the premise.

  "Centuries ago, people used an old dungeon in the nearby forest as a
  place for cultivating mushrooms.  Nowadays, the dungeon is seen as
  cursed, and has reportedly claimed the life of many an adventurer.
  Nobody knows how deep the it goes, or what exactly lies within its
  depths; but according to legend, a priceless treasure is to be found
  at the bottom.  Are you a bad enough dude to enter the dungeon?



---------- INSTALLATION

Samovar has two main dependencies: SBCL (https://www.sbcl.org/) and 
BearLibTerminal (http://foo.wyrd.name/en:bearlibterminal).  Place the
appropriate version of BearLibTerminal in the lib/ directory, and
then run `sh build.sh' to compile samovar into an executable.  Due
to how common lisp programs are built, the executable will be very
large, because the common lisp runtime is packaged into it.

The installation process should be made easier and more efficient in
future versions.



---------- CONTROLS

[escape] = quit, cancel action

[right, left, up, down, 1-9] = move around
[o] + direction = open door
[c] + direction = close door
[j] + direction = jam door
[k] + direction = kick door

[i] = examine item in inventory
[d] = drop item
[q] = drink tea
[e] = equip weapon/armor
[u] = unequip weapon/armor
[r] = read card
[b] = brew tea
[t] = throw item
[,] = pick up item from floor
[.] = rest (recharges health)
[/] = travel up/down stairs


---------- CONTENT

I have deliberately not documented the content of the dungeon — that
is, the different monsters, items, and structures that exist.  These
things are up for you to discover.  The game is very free-form, and
part of the fun is developing your own strategy.



---------- BUGS

Unfortunately, there are bugs.  Here are some that you may encounter:
1. spawning into a wall
2. no stairs
3. can't load savefile
Please report any bugs besides these to the maintainers.
