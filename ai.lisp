;;;; ai.lisp
;;;; artificial intelligence (pathfinding, behavior) for monsters

(in-package #:samovar)



(defun blink-monst (monst target &optional through)
  "Blink towards a target and animate the monster's motion."
  (lambda (path)
    (let ((len (length path)))
      (lambda (time)
        (when (and (not (eq (state monst) :dead))
                   (< time len))
          (let* ((c (nth time path))
                 (tile (apply #'get-tile c))
                 (m (<cell>-monst (apply #'get-cell c))))
            (print-monst monst)
            (when (equal m target) (attack monst m))
            (when (and (not (opaque tile))
                       (or (not m) (and through (eq m target))))
              (unplace-monst monst)
              (place-monst monst (first c) (second c))
              t)))))))


(defun monst-dead-p (m) (<= (hp m) 0.0))

(defun monst-low-health-p (m) (<= (hp m) (* 0.25 (max-hp m))))

(defun monst-flee (m)
  (monst-navigate m *player-inv-flow-field*))

(defun monst-adjacent-player-p (m)
  (<= (euclidean-dist (monst-coord *player*) (monst-coord m)) 2))

(defun monst-attack-player (m) (attack m *player*))

(defun monst-close-to-player-p (m)
  "Determine whether a monst is 'close' to a player (kind of
  arbitrary)"
  (let ((d (manhattan-dist (monst-coord *player*) (monst-coord m))))
    (<= d 5)))

(defun monst-sees-player-p (m)
  (let ((d (manhattan-dist (monst-coord *player*) (monst-coord m))))
    (if (stealth *player*) (<= d 2)
        (or (< d 5) (and (< d 10) (line-clear-p (monst-coord *player*)
                                                (monst-coord m)))))))

(defun monst-pursue-player (m)
  (if (test-flags m %keeps-distance)
      (let ((c (cost-at (monst-coord m) *player-flow-field*)))
        (when (and c (<= c 5))
          (monst-navigate m *player-inv-flow-field*)))
      (monst-navigate m *player-flow-field*)))


(defun monst-wander (m)
  "Wander around randomly."
  (let* ((dir (bearing m))
         (c (monst-coord m))
         (dc (dir-dx-dy dir))
         (next (add-coords c dc))
         (dirs (list :n :e :s :w :ne :se :sw :nw))
         (dirs (delete dir (delete (opposite-dir dir) dirs))))
    ;; if next cell if free along current path, go to it
    (if (and (walkable (apply #'get-tile next))
             (not (<cell>-monst (apply #'get-cell next))))
        (progn (move-monst m (first dc) (second dc))
               ;; straighten out from diagonal movement with a fairly
               ;; high probability
               (when (< (random 5) 2)
                 (setf (bearing m)
                       (case (bearing m)
                         (:se :s)
                         (:sw :w)
                         (:nw :n)
                         (:ne :e)
                         (t (bearing m))))))
        ;; if next cell isn't walkable, look for adjacent doors
        ;; and redirect towards the first one you find
        (unless (loop for d in dirs do
          (let* ((next (add-coords c (dir-dx-dy d)))
                 (tg (tag (apply #'get-tile next))))
            (when (member tg '(:closed-door :open-door :broken-door))
              (setf (bearing m) d)
              (return t))))
          ;; otherwise, pick a new, non-opposite direction at random
          (setf (bearing m) (pick-random dirs))))))
