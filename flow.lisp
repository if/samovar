;;;; flow.lisp
;;;; Code for calculating and working with 'flow fields', useful for
;;;; grid-based pathfinding.

(in-package #:samovar)



(defparameter *player-flow-field* nil
  "A flow field pointing towards the player")
(defparameter *player-inv-flow-field* nil
  "A flow field pointing away from the player")

;; thank you, redblobgames
(defun make-flow-field (start &optional initial-field)
  "Make a flow field originating at the specified point."
  (let ((q (priority-queue:make-pqueue #'<)) ; :key-type 'single-float
        (field (or initial-field
                   (make-array (list *width* *height*)
                               :initial-element nil))))
    ;; put in the `start' point
    (priority-queue:pqueue-push start 0.0 q)
    (setf (aref2 field start) 0.0)
    ;; do a breadth-first-ish search
    (loop until (priority-queue:pqueue-empty-p q) do
      (let* ((cur (priority-queue:pqueue-pop q))
             (nbs (filter (lambda (c) (walkable (apply #'get-tile c)))
                          (neighbor-coords cur))))
        (loop for next in nbs do
          (let ((c (flow-cost field cur next))
                (oldc (aref2 field next)))
            (when (or (not oldc) (< c oldc))
              (priority-queue:pqueue-push next c q)
              (setf (aref2 field next) c))))))
    field))


(defun make-reverse-flow-field (start orig-field)
  "Make a flow field that flows away from the given point, and
smoothly avoids corners."
  (let ((flipped-field (make-array (list *width* *height*)
                                   :initial-element nil)))
    ;; copy the original field, multiplying costs by a negative number
    (loop for i below *width* do
      (loop for j below *height*
            for c = (aref orig-field i j)
            when c do
              (setf (aref flipped-field i j) (* -2 c))))
    ;; then run the normal flow field algorithm on it
    (make-flow-field start flipped-field)))

(defun walkable-coord (coord)
  "Determine whether the tile at `coord' is walkable."
  (let* ((c (get-cell (first coord) (second coord)))
         (tl (<cell>-tile c)))
    (and (walkable tl)
         ;; (not (eq (tag tl) :closed-door))
         )))

(defun flow-cost (field cur next)
  "Calculate the cost of moving between tiles `cur' and `next'."
  ;; Slightly prioritize horizontal and vertical movement over
  ;; diagonal, because it looks better.  Also avoid other monsters.
  (cond
    ((or (= (first next) (first cur))
         (= (second next) (second cur)))
     (1+ (aref2 field cur)))
    (t (+ 1.2 (aref2 field cur)))))



(defun cost-at (coord field)
  "Look up the cost at `coord' in `field'"
  (aref2 field coord))



;; A* algorithm
(defun a-* (start end pred)
  "Return a list of points in the shortest path from `start' to `end'.
`pred' describes whether a point should be considered."
  (let ((q (priority-queue:make-pqueue #'<))
        (came-from (make-hash-table :test #'equalp))
        (cur-cost (make-hash-table :test #'equalp)))
    ;; put in the `start' point
    (priority-queue:pqueue-push start 0.0 q)
    (setf (gethash start cur-cost) 0.0)
    ;; do a breadth-first-ish search
    (loop until (priority-queue:pqueue-empty-p q) do
      (let* ((cur (priority-queue:pqueue-pop q))
             (nbs (filter pred (4-neighbor-coords cur))))
        (when (equalp cur end) (return))
        (loop for next in nbs do
          (let ((c (1+ (gethash cur cur-cost)))
                (oldc (gethash next cur-cost)))
            (when (or (not oldc) (< c oldc))
              (setf (gethash next cur-cost) c)
              (priority-queue:pqueue-push
               next
               (+ c (manhattan-dist next end))
               q)
              (setf (gethash next came-from) cur))))))
    (let ((p end))
      (loop until (not p)
            collect p
            do (setf p (gethash p came-from))))))
