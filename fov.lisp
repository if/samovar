(in-package #:samovar)

;; symmetric shadow-casting algorithm is from
;; https://www.albertford.com/shadowcasting/


(defstruct <row> depth start end)

(defun next-row (row)
  "Obtain the row one depth deeper than `row'"
  (make-<row> :depth (1+ (<row>-depth row))
              :start     (<row>-start row)
              :end       (<row>-end row)))

(defun in-quadrant (src q coord)
  "Return absolute coordinates, given coordinates
`coord' contained in quadrant `q' relative to the player "
  (let ((qx (first coord))
        (qy (second coord))
        (px (first src))
        (py (second src)))
    (case q
      (:north (list (+ px qy) (- py qx)))
      (:south (list (- px qy) (+ py qx)))
      (:east  (list (+ px qx) (+ py qy)))
      (:west  (list (- px qx) (- py qy))))))

(defun row-coords (row)
  "Get a list of coordinates of the tiles in some row"
  (let* ((d (<row>-depth row))
         (start-col (ceiling (- (* d (<row>-start row)) 0.5)))
         (end-col (floor (+ (* d (<row>-end row)) 0.5))))
    (loop for c from start-col to end-col
          collect (list d c))))

(defun wallp (src q coord)
  "Return whether a opaque tile is at `coord'"
  (when coord
    (destructuring-bind (x y)
        (in-quadrant src q coord)
      (opaque (get-tile x y)))))

(defun floorp (src q coord)
  "Return whether a transparent tile is at `coord'"
  (when coord
    (not (wallp src q coord))))

(defun symmetricp (row coord)
  "Return whether a tile can be seen symmetrically from origin"
  (let ((col (second coord))
        (d (<row>-depth row))
        (start (<row>-start row))
        (end (<row>-end row))) 
    (and (>= col (* start d))
         (<= col (* end d)))))


(defun calc-light-level (coord src radius)
  "Calculate light level based on distance of `coord' from `source'."
  (let* ((d (sqrt (euclidean-dist coord src)))
         ;; (d (manhattan-dist coord src))
         (q (/ d (/ radius 6.0))))
    (/ 1 (+ 1 (* q q)))))

(defun reveal (map q coord src radius)
  (let* ((cq (in-quadrant src q coord))
         (l (calc-light-level cq src radius)))
    (destructuring-bind (x y) cq
      (setf (aref map x y) l))))

(defun slope (coord)
  (destructuring-bind (x y) coord
    (/ (1- (* 2 y)) (* 2 x))))


(defun in-radius (src r2 q coord)
  "Determine whether `coord' in quadrant `q' is
within a radius whose square is `r2'"
  (when coord
    (destructuring-bind (x y) (in-quadrant src q coord)
      (let ((dx (- x (first src)))
            (dy (- y (second src))))
        (<= (+ (* dx dx) (* dy dy)) r2)))))

(defun scan (map src q radius row)
  (let ((prev nil))
    (loop for coord in (row-coords row)
          when (and (in-bounds map (in-quadrant src q coord))
                    (in-radius src radius q coord))
          do (when (symmetricp row coord)
               (reveal map q coord src radius))
             (when (and (wallp src q prev) (floorp src q coord))
               (setf (<row>-start row) (slope coord)))
             (when (and (floorp src q prev) (wallp src q coord))
               (let ((row2 (next-row row)))
                 (setf (<row>-end row2) (slope coord))
                 (scan map src q radius row2)))
             (setf prev coord))
    (when (floorp src q prev)
      (scan map src q radius (next-row row)))))


(defun light-quadrant (src map q radius)
  (scan map src q radius (make-<row> :depth 1 :start -1 :end 1)))

(defun lightmap (src radius)
  (let ((map (make-array (list *width* *height*)
                         :initial-element 0)))
    (loop for q in '(:north :east :south :west)
          do (light-quadrant src map q radius))
    (setf (aref2 map src) 1.0)
    map))
