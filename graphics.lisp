;;;; graphics.lisp

(in-package #:samovar)


;; a list of visible monsters
(defparameter *visible-monsts* nil)


;; viewpoint parameters
(defparameter *view-width* 75)
(defparameter *view-height* 30)
(defparameter *view-x1* 0)
(defparameter *view-y1* 0)
(defparameter *view-x2* *view-width*)
(defparameter *view-y2* *view-height*)


(defun update-viewport ()
  (setq *view-x1* (max 0 (- (x *player*) (floor *view-width* 2))))
  (setq *view-x2* (min *width* (+ *view-x1* *view-width*)))
  (setq *view-y1* (max 0 (- (y *player*) (floor *view-height* 2))))
  (setq *view-y2* (min *height* (+ *view-y1* *view-height*))))


(defun camera-print (i j s)
  "Print a tile with respect to a player-centered camera."
  (let* ((ox 25)
         (oy 0))
    (blt:print (+ ox (- i *view-x1*)) (+ oy (- j *view-y1*)) s)))




;; map with lighting effects
(defparameter *lighting* (make-array (list *width* *height*)
                                     :initial-element nil))

(defun set-light (coord dl dh)
  "Light a cell in the `*lighting*' map."
  (destructuring-bind (i j) coord
    (setf (aref *lighting* i j)
          (blend-lights (list dl dh)
                        (or (aref *lighting* i j) '(0 0))))))

(defun place-light (coord dh r &optional (fast t))
  "Put a point light on the lightmap.  `c' is the light's color at
maximum luminosity, and `fast' tells the function to forget about
the light if it's too far away from the player."
  (unless (and fast (> (manhattan-dist coord (monst-coord *player*)) 25))
    (let ((lights (lightmap coord r)))
      ;; iterate through lights
      (loop for i below *width* do
        (loop for j below *height*
              for dl = (aref lights i j)
              when (not (zerop dl)) do
                (set-light (list i j) dl dh))))))

(defun reset-lighting ()
  "Reset the `*lighting*' map to total darkness."
  (setq *lighting* (make-array (list *width* *height*)
                               :initial-element nil)))


;; narration text
;; the first item is the actual text, and the second is a 'dirty flag'
;; which says whether the status was updated in the previous iteration
(defparameter *statuses* nil)

(defun set-status (status)
  "Set current status to `status'"
  (push status *statuses*)
  (when (> (length *statuses*) 8)
    (setq *statuses* (butlast *statuses*))))

(defun clear-status ()
  (setq *statuses* nil))


(defun color-tile (tile &optional (dl 1.0) dh)
  "Set the fg and bg based on some tile"
  (setf (blt:color) (lighten (fg tile) dl dh))
  (setf (blt:background-color) (lighten (bg tile) dl dh)))

(defun color-monst (m &optional (dl 1.0))
  "Set the fg to color a monster"
  (setf (blt:color) (lighten (color m) dl)))


(defun print-monst (m &optional (dl 1.0))
  (color-monst m dl)
  (camera-print (x m) (y m) (ch m)))

(defun print-item (it i j dl)
  (setf (blt:color) (lighten (color it) dl))
  ;; (setf (blt:background-color) (bg (get-cell i j #'<cell>-tile)))
  (camera-print i j (ch it)))

(defun draw-cell (cell i j &optional (dl 0.0) dh)
  "Draw a depiction of a cell"
  (let ((tile (<cell>-tile cell))
        (items (<cell>-items cell))
        (monst (<cell>-monst cell)))
    (color-tile tile dl dh)
    (cond (monst (print-monst monst dl))
          (items (print-item (car items) i j dl))
          (t (camera-print i j (ch tile))))))


(defun light-cell (cell i j)
  (wlet ((l (light (<cell>-tile cell))))
    (place-light (list i j) (first l) (second l) nil))
  (when (<cell>-monst cell)
    (wlet ((l (light (<cell>-monst cell))))
      (place-light (list i j) (first l) (second l) nil))))

(defun draw-remembered-cell (cell i j)
  "Draw a cell that's not in sight, just in the player's memory."
  (setf (blt:background-color) +black+)
  (let ((items (<cell>-items cell))
        (ttag (tag (<cell>-tile cell))))
    (setf (blt:color)
          (if (and (not items)
                   (or (eq ttag :upstairs)
                       (eq ttag :downstairs)))
              +yellow+ (blt:gray :value 0.3)))
    (camera-print
     i j (cond
           ;; ((<cell>-monst cell)
           ;;  (setf (blt:color) +springgreen+)
           ;;  (ch (<cell>-monst cell)))
           (items (ch (car items)))
           (t (ch (<cell>-tile cell)))))))

(defun print-map ()
  (update-viewport)
  (setq *visible-monsts* nil)
  (setf (blt:background-color) +black+)
  (blt:clear-area 50 0 *view-width* *view-height*)
  (let* ((progress (/ *level* (float *numlevels*)))
         (fov (lightmap (monst-coord *player*)
                        (- 50 (* progress 45.0))))
         (xov (lightmap (monst-coord *player*)
                        (- 100 (* progress 80.0)))))
    (loop for i from *view-x1* below *view-x2* do
      (loop for j from *view-y1* below *view-y2* do
            (light-cell (get-cell i j) i j)))
    (loop for i from *view-x1* below *view-x2* do
      (loop for j from *view-y1* below *view-y2*
            for cell = (get-cell i j)
            for monst = (<cell>-monst cell)
            for tile = (get-tile i j)
            for mem = (remembered-tile i j)
            for fov-light = (aref fov i j)
            for extra-light = (aref *lighting* i j) do
              (cond
                ;; case 1: player can see tile
                ((not (zerop fov-light))
                 (when (and monst (not (eql monst *player*)))
                   (push monst *visible-monsts*))
                 (remember-tile i j)
                 (setf (<cell>-visible cell) t)
                 (if extra-light
                     (destructuring-bind (dl dh)
                         (blend-lights extra-light (list fov-light nil))
                       (draw-cell (get-cell i j) i j dl dh))
                     (draw-cell (get-cell i j) i j fov-light)))
                ;; case 2: tile is lit from really far away
                ((and (not (zerop (aref xov i j))) extra-light)
                 (setf (<cell>-visible cell) t)
                 (when monst (push monst *visible-monsts*))
                 (draw-cell (get-cell i j) i j 0.5 (second extra-light)))
                ;; case 3: player remembers tile
                (mem (draw-remembered-cell (get-cell i j) i j)
                     (setf (<cell>-visible cell) nil))))))
  (reset-lighting))


(defun print-meter (x y q limit)
  "Print a depiction of some fixed-range value, like health."
  (let* ((r (/ q (float limit)))
         (n= (round (* 10 r)))
         (n- (- 10 n=)))
    (if (<= r 0)
        (progn
          (setf (blt:color) +red+)
          (blt:print x y "**********"))
        (progn
          (setf (blt:color) +springgreen+)
          (blt:print x y (make-string n= :initial-element #\=))
          (setf (blt:color) +cornflower+)
          (blt:print (+ x n=) y (make-string n- :initial-element #\-))))))

(defun print-monst-profile (x y m)
  (print-meter x (1+ y) (hp m) (max-hp m))
  (setf (blt:color) +pink+)
  (let ((i 0))
    (loop for e being the hash-keys of (effects m)
            using (hash-value v)
          when (and v (<= i 3)) do
            (incf i)
            (blt:print (+ 12 x) (+ y i)
                       (if (= i 3)
                           (format nil "< ~a..." e)
                           (format nil "< ~a" e)))))
  (setf (blt:color) +white+)
  (blt:print x y (name m))
  (blt:print x (+ 2 y) (format nil "DF: ~a" (round (* 100 (df m)))))
  (blt:print x (+ 3 y) (format nil "DMG: ~a" (calc-damage m))))


(defun print-hud ()
  "Print a heads-up display containing important info"
  ;; clear the screen
  (setf (blt:background-color) +black+)
  (blt:clear)
  
  ;; print health info, effects, samovar status
  (setf (blt:color) +white+)
  (blt:print 1 1 (format nil "Level ~d" (1+ *level*)))

  ;; monster info
  (print-monst-profile 1 3 *player*)
  (when (<= (hp *player*) (* 0.25 (max-hp *player*)))
    (setf (blt:color) +yellow+)
    (blt:print 5 3 "<!>"))
  (loop for m in *visible-monsts* for i from 0
        when (<= i 5) do
          (print-monst-profile 1 (+ 8 (* 5 i)) m))

  ;; statuses
  (setf (blt:color) (blt:gray :value 0.3))
  (loop for i from 0 below 100 do
    (blt:print i 30 "—"))
  (loop for s in *statuses* for i from 0 do
    (blt:print 1 (- 38 i) s)
    (setf (blt:color) (blt:gray :value 0.5))
          initially (setf (blt:color) +yellow+)))




(defparameter *animation-queue* (cl-speedy-queue:make-queue 255)
  "A queue of animation functions, for sequential special effects.
  The whole queue is emptied out every time the play scene is
  redrawn.")

;; NOTE: doesn't enqueue if `*animation-queue*' is full.
(defun enqueue-animation (anim)
  (unless (cl-speedy-queue:queue-full-p *animation-queue*)
      (cl-speedy-queue:enqueue anim *animation-queue*)))

(defun dequeue-animation ()
  (cl-speedy-queue:dequeue *animation-queue*))

(defun animations-empty-p ()
  (cl-speedy-queue:queue-empty-p *animation-queue*))


(defun run-animation (anim)
  "Run an animation function, which executes in steps and returns
`nil' when finished."
  (loop for time from 0
        while (funcall anim time)
        do (blt:refresh)
           (print-map)))



;; magic mapping effect
(defun magic-map-animation ()
  "'Memorize' the whole level, starting from the player's position,
and display an animation to go along with it."
  (breadth-first-search
   (monst-coord *player*)
   (lambda (c)
     (let* ((tl (apply #'get-tile c))
            (tg (tag tl)))
       (not (or (eq tg :wall)
                (eq tg :wall-lichen)
                (eq tg :wall-fungus)
                (eq tg :wall-smooth)))))
   :with-tile (lambda (c)
                (apply #'remember-tile c)
                (setf (blt:background-color) +black+)
                (setf (blt:color) +springgreen+)
                (camera-print
                 (first c) (second c)
                 (make-string 1 :initial-element
                              ;; (rand-char #x2f00 #x2fd5) ; kangxi radicals
                              (rand-char #x2800 #x283f) ; braille
                              )))
   :before-turn (lambda () (print-map))
   :after-turn (lambda () (sleep 0.01) (blt:refresh))))
