;;;; items.lisp
;;;; defines the different items that can be used in the game,
;;;; as well as utilities for using the inventory.

(in-package #:samovar)


;; the player's inventory.
(defparameter *inv* (make-array 26 :initial-element nil))


(defclass <item> ()
  ((name :initform "useless item" :accessor name)
   (desc :initform "a useless item" :accessor desc)
   (stackable :initform nil :accessor stackable)
   (color :initform +yellow+ :accessor color)
   (ch :initform "?" :accessor ch)
   (light :initform nil :accessor light)))



(defmethod throw-animate-item ((it <item>) start path time)
  "Animate an item being thrown."
  (cond
    ;; if path is empty, plonk item onto the ground and stop
    ((not path) (apply #'place-item it start) nil)
    ;; if at end of path, plonk onto ground and stop
    ((= time (length path))
     (destructuring-bind (x y) (car (last path))
       ;; (place-item it x y)
       (finish-throw-item it (list x y))
       nil))
    ;; if next thing in path is an obstruction, stop
    ((destructuring-bind (x y) (nth time path)
       (or (get-cell x y #'<cell>-monst)
           (opaque (get-cell x y #'<cell>-tile))))
     ;; (apply #'place-item it (if (zerop time) start
     ;;                            (nth (1- time) path)))
     (finish-throw-item it (nth time path))
     nil)
    ;; otherwise, just travel along
    (t (destructuring-bind (x y) (nth time path)
         (print-item it x y 1.0)
         t))))

(defmethod finish-throw-item ((it <item>) coord)
  "Hook to run when an item finishes traveling thru the air."
  (apply #'place-item it coord))

(defmethod throw-item ((it <item>) src dest)
  "Throw an item located at `src' to point `dest'.  The item's flight
is animated, and the item lands at or before the destination,
depending on whether the path is clear."
  (let* ((animate (lambda (path)
                    (lambda (time)
                      (throw-animate-item it src path time))))
         (b (make-<bolt> :start src :end dest
                         :animate animate)))
    (launch-bolt b)))

(defmethod format-item ((it <item>))
  (format nil "~a" (name it)))

;; used for stacking items
(defmethod items-equal ((i1 <item>) (i2 <item>))
  "Determine whether two items can be considered 'equal' in value."  
  (eq (class-of i1) (class-of i2)))

;; very similar to a scroll in typical roguelikes
(defclass <card> (<item>)
  ((name  :initarg :name  :accessor name)
   (desc  :initarg :desc  :accessor desc)
   (magic :initarg :magic :accessor magic)
   (stackable :initform t)
   (ch :initform "?")
   (color :initform +cyan+)))

(defmethod items-equal ((c1 <card>) (c2 <card>))
  (eq c1 c2))

(defmethod read-card ((c <card>))
  (funcall (magic c)))

(defparameter +card-teleportation+
  (make-instance '<card> :name "Card of teleportation"
                         :desc "Teleports you to a random location"
                         :magic (lambda ()
                                  (apply #'place-monst *player*
                                         (find-walkable-place))
                                  (set-status "You teleport somewhere."))))

(defparameter +card-purification+
  (make-instance '<card> :name "Card of purification"
                         :desc "Cleans the surroundings of blood and mycelium"
                         :magic (lambda ()
                                  (loop for i from (- (x *player*) 5) to (+ (x *player*) 5) do
                                    (loop for j from (- (y *player*) 5) to (+ (y *player*) 5)
                                          when (in-bounds *map* (list i j)) do
                                            (case (tag (get-tile i j))
                                              (:mycelium (set-tile i j +luminescent-fungus+))
                                              (:blood (set-tile i j +floor+))
                                              (:dead-plant (set-tile i j +luminescent-fungus+)))))
                                  (set-status "The ground around you becomes clean."))))

(defparameter +card-spark+
  (make-instance '<card> :name "Card of throw spark"
                         :desc "Lets you throw a spark somewhere"
                         :magic (lambda ()
                                  (wlet ((dest (tile-select (monst-coord *player*))))
                                    (cast-spark (monst-coord *player*) dest 10)
                                    (set-status "A spark flies out of your hands!")))))

(defparameter +card-c-beam+
  (make-instance '<card> :name "Card of cast C-beam"
                         :desc "Shoots out a deadly C-beam.  Use it wisely!"
                         :magic (lambda ()
                                  (wlet ((dest (tile-select (monst-coord *player*))))
                                    (cast-c-beam (monst-coord *player*) dest 20)
                                    (set-status "A C-beam shoots out of your hands!")))))

(defparameter +card-blink+
  (make-instance '<card> :name "Card of blinking"
                         :desc "Lets you blink to a new location."
                         :magic (lambda ()
                                  (wlet ((dest (tile-select (monst-coord *player*))))
                                    (when (line-clear-p (monst-coord *player*) dest)
                                      (blink *player* dest)
                                      (set-status "You blink to a new position."))))))

(defparameter +card-magic-mapping+
  (make-instance '<card> :name "Card of magic mapping"
                         :desc "Reveals the layout of current level."
                         :magic (lambda ()
                                  (magic-map-animation)
                                  (set-status "You have a vision of the dungeon's layout."))))



;; (defparameter +card-vines+
;;   (make-instance
;;    '<card>
;;    :name "Card of cast vines"
;;    :desc "Lets you grow vines."
;;    :magic (lambda ()
;;             (wlet ((dest (tile-select (monst-coord *player*))))
;;               (cast-vines (monst-coord *player*) dest)
;;               (set-status "A thick cord of vines materializes!")))))

(defparameter +card-analyze+
  (make-instance
   '<card>
   :name "Card of analysis"
   :desc "Breaks apart symbiotic monsters."
   :magic (lambda ()
            (loop for m in *visible-monsts* do (analyze m))
            (set-status "You feel uncomfortable."))))


(defun blink (monst dest)
  (let ((src (monst-coord monst)))
    ;; forgive me for this horrible naming scheme
    (flet ((blink! (path)
             (lambda (time)
               (when (and (not (eq (state monst) :dead))
                          (< time (length path)))
                 (let* ((c (nth time path))
                        (tile (apply #'get-tile c))
                        (m (<cell>-monst (apply #'get-cell c))))
                   (print-monst monst)
                   (when (and (not (opaque tile)) (not m))
                     (unplace-monst monst)
                     (place-monst monst (first c) (second c))
                     t))))))
      (when (not (eq (state monst) :dead))
        (launch-bolt (make-<bolt> :start src :end dest :animate #'blink!))))))

(defun cast-vines (start end)
  (flet ((vines! (path)
           (lambda (time)
             (when (< time (length path))
               (let* ((c (nth time path))
                      (blocked (opaque (apply #'get-tile c))))
                 (unless blocked
                   (set-tile (first c) (second c) +vines+)))))))
    (launch-ray (make-<bolt> :start start :end end :animate #'vines!))))

(defun cast-spark (start end damage)
  (flet ((spark! (path)
           (lambda (time)
             (when (< time (length path))
               (let ((c (nth time path))
                     (q (/ (float time) (length path))))
                 ;; hurt any monsters who are in the way
                 (wlet ((m (get-cell (first c) (second c) #'<cell>-monst)))
                   (damage-monst m damage t))
                 ;; draw the spark
                 (setf (blt:background-color) (hsv2rgb (list 240 (- 1 q) 1.0 #xff)))
                 (place-light c 240 50 nil)
                 (camera-print (first c) (second c) " ")
                 ;; continue bolt if tile isn't opaque
                 (not (opaque (apply #'get-tile c))))))))
    (launch-ray (make-<bolt> :start start :end end :animate #'spark!))))

(defun cast-c-beam (start end damage)
  (let* ((damage (floor damage 5))      ; C-beams are 5x slow, so do 5x damage
         (twist '("⠏" "⠎" "⠞" "⠟" "⠮" "⠯" "⠾" "⠿"))
         (twist-len (length twist))
         (color +magenta+)
         (streamers '(":" "-" "_" "=")))
    (flet ((c-beam! (path)
             (let* ((l (length path))
                    (l5 (* 5 l)))
               (lambda (time)
                 (when (< time l5)
                   (let ((c (nth (floor time 5) path)))

                     ;; hurt any monsters who are in the way
                     (wlet ((m (get-cell (first c) (second c) #'<cell>-monst)))
                       (damage-monst m damage t))
                     
                     ;; draw the beam
                     (place-light c 300 50 nil)
                     (setf (blt:background-color) +black+)
                     (setf (blt:color) color)
                     (loop for coord in path for i from 0
                           for dist = (- (min l time) i)
                           when (and (<= i time) (< dist 10)) do
                             (camera-print (first coord) (second coord)
                                           (if (< dist 7)
                                               (nth (mod i twist-len) twist)
                                               (pick-random streamers))))

                     ;; continue bolt if tile isn't opaque
                     (not (opaque (apply #'get-tile c)))))))))
      (launch-ray (make-<bolt> :start start :end end :animate #'c-beam!)))))




(defclass <mellifluous-jasmine> (<item>)
  ((name  :initform "mellifluous jasmine")
   (desc  :initform "A rare jasmine plant, said to have come directly from heaven.")
   (ch    :initform "花")
   (color :initform +pink+)))

(defparameter +mellifluous-jasmine+
  (make-instance '<mellifluous-jasmine>)
  "The one and only.")

(defclass <tea> (<item>)
  ((name :initarg :name)
   (desc :initform "A bottle of tea.")
   (stackable :initform t)
   (ch :initform "!")
   (effect :initarg :effect :accessor effect)))

(defmethod items-equal ((t1 <tea>) (t2 <tea>))
  (eq (effect t1) (effect t2)))

(defmethod apply-tea-effect ((tea <tea>) m)
  "Apply the effect of tea to a monster."
  (apply-effect
   (effect tea)
   (case (effect tea)
     (:health 1)
     (:harm 1)
     (:cure 1)
     (:nothing 1)
     (:swill 1)
     (t 45))
   m))

(defun apply-effect (e duration m)
  (unless (or (has-effect :contamination m)
              (and (has-effect :resistance m)
                   (eq e :poison)))
    ;; if the monster already has this effect, add the duration
    (setf (gethash e (effects m))
          (+ (or (gethash e (effects m)) 0.0)
             duration))
    (start-effect e m)))


(defmethod finish-throw-item ((tea <tea>) coord)
  (set-status "The flask shatters on the ground!")
  
  (case (effect tea)
    (:swill (let ((coords (mapcar (lambda (c) (add-coords c coord)) +circle-3+)))
               (dolist (c coords)
                 (when (and (in-bounds *map* c)
                            (walkable (apply #'get-tile c))
                            (< (random 1.0) 0.6))
                   (set-tile (first c) (second c) +swill+)))))
    (:poison (wlet ((m (<cell>-monst (apply #'get-cell coord))))
               (apply-effect :poison 10 m)))
    (:harm (wlet ((m (<cell>-monst (apply #'get-cell coord))))
             (apply-effect :harm 10 m)))))

(defclass <tea-leaves> (<item>)
  ((stackable :initform t :accessor stackable)
   (name :initarg :name :accessor name)
   (desc :initarg :desc :accessor desc)
   (color :initarg :color :accessor color)
   (ch :initform "茶" :accessor ch)
   (effect :initarg :effect :accessor effect)))

(defmethod items-equal ((i1 <tea-leaves>) (i2 <tea-leaves>))
  "Determine whether two items can be considered 'equal' in value."  
  (eq i1 i2))



(defparameter +genmaicha+
  (make-instance '<tea-leaves> :name "Genmaicha"
                               :desc "A warm, nutty green tea with brown rice."
                               :effect :health
                               :color +gold+))

(defparameter +sencha+
  (make-instance '<tea-leaves> :name "Sencha"
                               :desc "Grassy green tea."
                               :effect :cure
                               :color +springgreen+))

(defparameter +oolong-tea+
  (make-instance '<tea-leaves> :name "Oolong tea"
                               :desc "A complex, fruity tea."
                               :effect :invisibility
                               :color +sky-blue+))

;; TODO: add some special/rare teas

;; (defparameter +linden-tea+
;;   (make-instance '<tea-leaves> :name "Linden tea"
;;                                :desc "A sweet, buttery herbal tea."
;;                                :effect :lignification
;;                                :color +orange+))

;; (defparameter +rooibos-tea+
;;   (make-instance '<tea-leaves> :name "Rooibos tea"
;;                                :desc "An earthy tea with a tobacco-like flavor."
;;                                :effect :stanch
;;                                :color +red+))



;; (defparameter +longjing+
;;   (make-instance '<tea-leaves> :name "longjing tea"
;;                                :desc "pan-roasted green tea"
;;                                :effect :health
;;                                :color (blt:rgba #x99 #xab #x47)))


;; (defparameter +bai-lin+
;;   (make-instance '<tea-leaves> :name "bai lin tea"
;;                                :desc "black tea with a warm flavor"
;;                                :effect :health
;;                                :color (blt:rgba #x8e #x66 #x42)))

;; (defparameter +moonlight-tea+
;;   (make-instance '<tea-leaves> :name "moonlight tea"
;;                                :desc "a soft and sweet white tea"
;;                                :effect :health
;;                                :color (blt:rgba #xd8 #xca #x8d)))

;; (defparameter +cornflower-tea+
;;   (make-instance '<tea-leaves> :name "cornflower tea"
;;                                :desc "citrusy black tea with cornflowers"
;;                                :effect :health
;;                                :color +cornflower+))


;; (defparameter +lapsang-souchong+
;;   (make-instance '<tea-leaves> :name "lapsang souchong tea"
;;                                :desc "a smoky black tea"
;;                                :effect :health
;;                                :color (blt:rgba #x9e #x82 #x76)))

;; (defparameter +jasmine-tea+
;;   (make-instance '<tea-leaves> :name "jasmine tea"
;;                                :desc "tea with a jasmine flavor"
;;                                :effect :health
;;                                :color (blt:rgba #xea #xe3 #xd4)))


(defun brew-tea (effect name)
  "Bre `<tea>' with a certain `<effect>'."
  (make-instance '<tea>
                 :effect effect
                 :name name))



(defclass <weapon> (<item>)
  ((ch   :initform "/" :accessor ch)
   (dmg  :initarg :dmg  :accessor dmg)
   ;; (level :initform 0 :accessor level)
   (acc-bonus :initarg :acc-bonus :initform 0.0 :accessor acc-bonus)
   (name :initarg :name :accessor name)
   (desc :initarg :desc :accessor desc)
   (equipped :initform nil :accessor equipped)))

;; (defmethod enchant ((w <weapon>))
;;   (incf (level w))
;;   (incf (dmg w))
;;   (incf (acc-bonus w) 0.05))

(defun make-random-weapon (min-damage max-damage &optional (max-acc 0.0))
  "Generate a weapon with a random name and random traits, within the
specified limits"
  (let* ((d (round (+ min-damage (random (- max-damage min-damage)))))
         (acc (random max-acc))
         (material (cond ((> d 20) (pick-random '("Obsidian " "Crystal ")))
                         ((> d 15) (pick-random '("Steel " "Iron ")))
                         ((> d 5)  (pick-random '("Bronze " "Jade ")))
                         (t "")))
         (type (pick-random '("Dagger" "Hammer" "Club" "Sword" "Pike" "Staff")))
         (name (format nil "~a~a~a"
                       (cond ((> acc 0.8) "Legendary ")
                             ((> acc 0.5) "Fearsome ")
                             ((> acc 0.2) "Lucky ")
                             (t ""))
                       material type))
         (desc (format nil "A weapon that deals ~d damage, with a ~d% bonus to accuracy"
                       d (round (* 100 acc)))))
    (make-instance '<weapon> :acc-bonus acc :dmg d :name name :desc desc)))


;; special weapons — only one of each exists within a given game
(defparameter +assassin-dagger+
  (make-instance '<weapon> :acc-bonus 0.5 :dmg 10
                           :name "Assassin's dagger"
                           :desc "A small and exceptionally ornate dagger"))

(defparameter +naginata+
  (make-instance '<weapon> :acc-bonus 0.5 :dmg 20
                           :name "Naginata"
                           :desc "A curved, single-edged blade mounted on a pole"))

(defparameter +flamberge+
  (make-instance '<weapon> :acc-bonus 1.0 :dmg 30
                           :name "Flamberge"
                           :desc "A two-handed sword with an undulating blade"))



(defmethod throw-animate-item ((w <weapon>) start path time)
  (let* ((c (nth time path))
         (monst (and c (get-cell (first c) (second c) #'<cell>-monst))))
    ;; if the weapon hits a monster, damage the monster and let the
    ;; garbage collector take care of the weapon
    (if (and monst (damage-monst monst (dmg w)))
        nil
        ;; otherwise, travel like a normal item
        (call-next-method))))


(defmethod equip ((w <weapon>))
  (when (weapon *player*) (unequip (weapon *player*)))
  (setf (weapon *player*) w)
  (setf (equipped w) t)
  (update-equipment *player*))

(defmethod unequip ((w <weapon>))
  (setf (weapon *player*) nil)
  (setf (equipped w) nil)
  (update-equipment *player*))

(defmethod format-item ((w <weapon>))
  (format nil "~a (~a)"
          (name w)
          (if (equipped w)
              "[color=yellow]equipped[/color]"
              "unequipped")))

;; `weapon-attack' describes how attacking with weapon `w' works, and
;; is used for special behavior, such as attacking from two tiles away
;; with a spear.
;; 
;; default attack method:
(defmethod weapon-attack ((w t) m coord dir)
  (wlet ((m2 (get-cell (first coord) (second coord)
                       #'<cell>-monst)))
    (attack m m2)
    t))


(defclass <quarterstaff> (<weapon>)
  ((name :initform "Quarterstaff")
   (desc :initform "A wooden staff used for combat")
   (dmg :initform 5)))

(defclass <javelin> (<weapon>)
  ((name :initform "Javelin")
   (ch :initform "\\")
   (desc :initform "Essentially a long, pointy stick")
   (dmg :initform 7)
   (stackable :initform t)))

(defmethod items-equal ((j1 <javelin>) (j2 <javelin>))
  (= (dmg j1) (dmg j2)))

(defclass <pike> (<weapon>)
  ((name :initform "Pike")
   (desc :initform "A long staff with a pointed blade at the end")
   (dmg :initform 12)))

(defmethod weapon-attack ((w <pike>) m coord dir)
  ;; attack the closest cell
  (let* ((p1 (call-next-method))
         ;; also attack the next cell
         (c2 (add-coords coord dir))
         (p2 (wlet ((m2 (get-cell (first c2) (second c2)
                                  #'<cell>-monst)))
               (attack m m2))))
    (or p1 p2)))


(defclass <scythe> (<weapon>)
  ((name :initform "Scythe")
   (desc :initform "A long staff with a curved blade at the end")
   (dmg :initform 10)))

;; with a scythe, you can attack all monsters on your sides, but not
;; the one you're moving directly towards.
(defmethod weapon-attack ((w <scythe>) m c dir)
  (let ((nbs (filter
              (lambda (c2) (not (equal c c2)))
              (neighbor-coords (monst-coord m)))))
    (loop for (i j) in nbs
          for m2 = (get-cell i j #'<cell>-monst)
          when m2 do
            (attack m m2))))


(defclass <armor> (<item>)
  ((ch :initform "[[" :accessor ch)
   (df   :initarg :df   :accessor df)
   ;; (level :initform 0 :accessor level)
   (name :initarg :name :accessor name)
   (desc :initarg :desc :accessor desc)
   (equipped :initform nil :accessor equipped)))


;; (defmethod enchant ((a <armor>))
;;   (incf (level a))
;;   (incf (df a) 0.05))

(defun make-random-armor (min-df max-df)
  "Generate armor with a random name and random traits, within the
specified limits"
  (let* ((d (+ min-df (random (- max-df min-df))))
         (material (cond ((> d 0.6) (pick-random '("Steel " "Iron ")))
                         ((> d 0.2) (pick-random '("Bronze " "Copper ")))
                         (t (pick-random '("Wool " "Bark ")))))
         (type (cond ((> d 0.5) (pick-random '("Mail" "Plate armor")))
                     (t (pick-random '("Tunic" "Shirt")))))
         (name (format nil "~a~a" material type))
         (desc (format nil "Offers ~d% defense against melee attacks."
                       (round (* 100 d)))))
    (make-instance '<armor> :df d :name name :desc desc)))

;; one-of-a-kind armor
(defparameter +magical-robes+
  (make-instance '<armor> :df 0.5 :name "Embroidered Robes"
                          :desc "Special robes that ward off half of all melee attacks"))

(defparameter +mithril-mail+
  (make-instance '<armor> :df 0.8 :name "Mithril Mail"
                          :desc "A lightweight shirt of glimmering mithril"))




(defmethod throw-item ((a <armor>) src dest)
  (when (equipped a) (unequip a))
  (call-next-method))

(defmethod equip ((a <armor>))
  (when (armor *player*) (unequip (armor *player*)))
  (setf (armor *player*) a)
  (setf (equipped a) t)
  (update-equipment *player*))

(defmethod unequip ((a <armor>))
  (setf (armor *player*) nil)
  (setf (equipped a) nil)
  (update-equipment *player*))


(defmethod format-item ((a <armor>))
  (format nil "~a (~a)"
          (name a)
          (if (equipped a)
              "[color=yellow]equipped[/color]"
              "unequipped")))

(defclass <flaxen-tunic> (<armor>)
  ((name :initform "Flaxen Tunic")
   (desc :initform "A flimsy tunic, made of flax.")
   (df   :initform 0.05)))


(defun inv-index-char (i)
  "Convert an inventory index number to an alphabetical char"
  (code-char (+ i 97)))

(defun inv-char-index (c)
  "Convert an inventory slot letter to the corresponding index"
  (- (char-code c) 97))

(defun stack-item (item index)
  "Add an `item' to the inventory, stacking it with other items
in the slot given by `index'"
  (push item (aref *inv* index)))

(defun find-stack-index (item)
  "Find the proper inventory slot to stack `item' in,
defaulting to the first open slot if there is no existing stack."
  (or (position-if (lambda (x)
                     (and x (listp x) (items-equal item (car x))))
                   *inv*)
      (position nil *inv*)))

(defun inventory-add (item)
  "Add an item to the first open slot of the player's inventory"
  (let* ((stack (stackable item))
         (i (if stack (find-stack-index item)
                (position nil *inv*))))
    (cond ((not i) (error "TODO: handle inventory size limit"))
          (stack (stack-item item i))
          (t (setf (aref *inv* i) item)))))

(defun inventory-peek (i)
  "Retrieve (but don't remove) an item from the inventory.  If the
item is in a stack, return the first item in the stack.  If there is
no item in the given position, return `nil'."
  (let ((it (aref *inv* i)))
    (if (listp it) (car it) it)))

(defun inventory-remove (i)
  "Remove the i'th item from inventory and return it"
  (let ((it  (aref *inv* i)))
    (if (and it (listp it))
        (pop (aref *inv* i))
        (progn (setf (aref *inv* i) nil) it))))

(defun take-item (x y)
  "Remove an item from the map and store it in the player's inventory"
  (wlet ((it (pop (<cell>-items (get-cell x y)))))
    (inventory-add it)
    (set-status (format nil "Picked up item: ~a." (name it)))))

(defun place-item (it x y)
  (push it (<cell>-items (aref *map* x y))))

(defun drop-item (i)
  "Remove an item from inventory and drop it on the ground"
  (wlet ((it (inventory-peek i)))
    (inventory-remove i)
    (place-item it (x *player*) (y *player*))
    ;; TODO: make this less messy
    (when (and (or (typep it '<armor>) (typep it '<weapon>))
               (equipped it))
      (unequip it))
    (set-status (format nil "Dropped item: ~a." (name it)))))


;; NOTE: references may be changed when a game is restored!
(defun inventory-find (obj)
  "Find the index of `obj' in the inventory, returning `nil' if
the object could not be found.  Uses equality of reference."
  (position-if
   (lambda (it)
     (and it (if (listp it) (eq (car it) obj) (eq it obj))))
   *inv*))

(defun remove-item-ref (obj)
  "Given an object, remove it from the inventory."
  (let ((i (inventory-find obj)))
    (inventory-remove i)))

(defun drop-item-ref (obj)
  "Given an object, drop it if it's in the inventory"
  (let ((i (inventory-find obj)))
    (drop-item i)))




;;;; TODO: put this stuff somewhere better

(defstruct <bolt>
  ;; function for drawing special effects and updating dungeon
  animate
  start end)

(defun launch-bolt (b)
  "Launch a bolt towards its destination.  Queues up an animation
event and applies whatever effects the bolt has."
  (let ((path (bresenham-bolt b)))
    (enqueue-animation (funcall (<bolt>-animate b) path))))

(defun launch-ray (b)
  "Like `launch-bolt', but uses a ray."
  (let ((path (bresenham-ray (<bolt>-start b) (<bolt>-end b))))
    (enqueue-animation (funcall (<bolt>-animate b) path))))


;; http://roguebasin.com/index.php/Bresenham%27s_Line_Algorithm
;; TODO: refine, abstract, etc
(defun bresenham-bolt (b)
  (bresenham-line (<bolt>-start b) (<bolt>-end b)))


(defun bresenham-line (start end)
  "Calculate coordinates of a linear 'bolt', stopping or reflecting
upon contact with obstacles.  The line begins right after the bolt's
`start' coordinate, and ends at the `end' coordinate."
  (let* ((x0 (first start))
         (x1 (first end))
         (y0 (second start))
         (y1 (second end))
         (steep (> (abs (- y1 y0)) (abs (- x1 x0)))))
    (when steep (swap x0 y0) (swap x1 y1))
    (let* ((backwards (> x0 x1))
           (ystep (if (< y0 y1) 1 -1))
           (dx (abs (- x1 x0)))
           (dy (abs (- y1 y0)))
           (err (truncate (/ dx 2)))
           (y y0)
           (path
             (if backwards
                 (loop for x from x0 downto x1
                       ;; choose coord ordering based on steepness
                       for c = (if steep (list y x) (list x y))
                       collect c
                       do (decf err dy)
                       when (< err 0) do
                         (incf y ystep)
                         (incf err dx))
                 (loop for x from x0 to x1
                       ;; choose coord ordering based on steepness
                       for c = (if steep (list y x) (list x y))
                       collect c
                       do (decf err dy)
                       when (< err 0) do
                         (incf y ystep)
                         (incf err dx)))))
      (cdr path))))


(defun bresenham-ray (start end)
  "Calculate coordinates of a ray using Bresenham's algorithm.  The
ray is defined as beginning at `start', going through `end', and
terminating at the edge of the map."
  (let* ((x0 (first start))
         (x1 (first end))
         (y0 (second start))
         (y1 (second end))
         (steep (> (abs (- y1 y0)) (abs (- x1 x0)))))
    (when steep (swap x0 y0) (swap x1 y1))
    (let* ((backwards (> x0 x1))
           (ystep (if (< y0 y1) 1 -1))
           (dx (abs (- x1 x0)))
           (dy (abs (- y1 y0)))
           (err (truncate (/ dx 2)))
           (y y0)
           (path
             (if backwards
                 (loop for x downfrom x0
                       ;; choose coord ordering based on steepness
                       for c = (if steep (list y x) (list x y))
                       while (in-bounds *map* c)
                       collect c
                       do (decf err dy)
                       when (< err 0) do
                         (incf y ystep)
                         (incf err dx))
                 (loop for x from x0
                       ;; choose coord ordering based on steepness
                       for c = (if steep (list y x) (list x y))
                       while (in-bounds *map* c)
                       collect c
                       do (decf err dy)
                       when (< err 0) do
                         (incf y ystep)
                         (incf err dx)))))
      (cdr path))))
