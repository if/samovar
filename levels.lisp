;;;; levels.lisp
;;;; for storing, saving, and loading levels.

(in-package #:samovar)

(defstruct <level>
  map
  upstairs
  downstairs
  monsts)


(defparameter *numlevels* 14)
(defparameter *levels* (make-array *numlevels* :initial-element nil))


(defun save-cur-level ()
  "Save the current level in `*levels*' at index `*level*'."
  ;; zap the player off the map so that it's not copied
  (setf (<cell>-monst (get-player-cell)) nil)
  (let ((l (make-<level> :map *map*
                         :upstairs *upstairs-pos*
                         :downstairs *downstairs-pos*
                         :monsts *monsts*)))
    (setf (aref *levels* *level*) l)))

(defun load-level (n)
  "Load the `nth' level.  Note that this doesn't save the current
level, so any unsaved level data will be overwritten."
  (let ((l (aref *levels* n)))
    (setq *map* (<level>-map l))
    (setq *upstairs-pos* (<level>-upstairs l))
    (setq *downstairs-pos* (<level>-downstairs l))
    (setq *monsts* (<level>-monsts l))))


(defstruct <saved-game>
  levels
  cur-level
  player
  inventory
  progress)


(defun save-game (path)
  "Save an incomplete game in a savefile located at `path'."
  ;; save the current level into the levels array
  (save-cur-level)
  (let* ((save (make-<saved-game>
                :levels *levels*
                :cur-level *level*
                :player *player*
                :inventory *inv*
                :progress *progress*)))
    (cl-store:store save path)))


(defun restore-game (path)
  "Restore a saved game."
  (let* ((save (cl-store:restore path)))
    (setq *levels* (<saved-game>-levels save))
    (setq *level* (<saved-game>-cur-level save))
    (setq *progress* (<saved-game>-progress save))
    (setq *player* (<saved-game>-player save))
    (setq *inv* (<saved-game>-inventory save))
    (load-level *level*)))




;; procedural generation recipes for every level
(defparameter *level-recipes*
  (list (list :plants 0.2
              :monsts '(5 ((<cricket-spider> <cricket-spider> <cricket-spider>)
                           <white-mushroom>)))
        (list :plants 0.3
              :monsts '(5 ((<cricket-spider> <cricket-spider> <cricket-spider>)
                           <white-mushroom> <enoki>))
              :challenge-room (list (cons 'treasure +assassin-dagger+)
                                    (cons 'monster '<greater-enoki>)))
        (list :plants 0.2
              :fungus 0.3
              :monsts '(6 (<white-mushroom> <greater-enoki>
                           (<enoki> <enoki> <enoki>))))
        (list :fungus 0.3
              :wall-lichen 0.3
              :monsts '(6 (<greater-enoki> <spider-cricket>
                           (<enoki> <enoki> <enoki>)))
              :challenge-room (list (cons 'treasure +assassin-dagger+)
                                    (cons 'monster '<redcap>)))
        (list :fungus 0.3
              :wall-lichen 0.2
              :mycelium 0.2
              :monsts '(10 ((<spider-cricket> <spider-cricket>)
                            <amanita> <white-mushroom>)))
        (list :fungus 0.2
              :mycelium 0.3
              :monsts '(5 (<enoki> <gardener>))
              :challenge-room (list (cons 'treasure +card-spark+)
                                    (cons 'monster '<porcelain-creature>)))
        (list :lights 0.3
              :mycelium 0.3
              :straw 0.2
              :monsts '(5 (<gardener> (<amanita> <amanita>))))
        (list :lights 0.2
              :mycelium 0.2
              :straw 0.3
              :monsts '(4 ((<straw-mushroom> <straw-mushroom> <straw-mushroom>
                            <death-cap>) <cricket-spider>))
              :challenge-room (list (cons 'treasure +magical-robes+)
                                    (cons 'monster '<spaghetti-worm>)))
        (list :straw 0.3
              :wall-fungus 0.2
              :monsts '(5 ((<straw-mushroom> <straw-mushroom> <straw-mushroom>
                            <death-cap>) <glow-worm>)) )
        (list :mycelium 0.3
              :wall-fungus 0.3
              :monsts '(5 ((<glow-worm> <glow-worm> <glow-worm>)
                           (<spaghetti-worm> <spaghetti-worm> <spaghetti-worm>)))
              :challenge-room (list (cons 'treasure +naginata+)
                                    (cons 'monster '<wood-ear>)))
        (list :wall-lichen 0.3
              :plants 0.3
              :monsts '(10 (<wood-ear> <death-cap> <porcelain-knight>)))
        (list :plants 0.3
              :monsts '(7 ((<wood-ear> <wood-ear>)
                           (<porcelain-knight> <porcelain-knight>)))
              :challenge-room (list (cons 'treasure +mithril-mail+)
                                    (cons 'monster '<lace-creature>)))
        (list :straw 0.3
              :wall-fungus 0.4
              :monsts '(10 (<gardener> <redcap> <bleeding-tooth>)))
        (list :mycelium 0.3
              :monsts '(10 (<redcap> <lace-creature> <greater-enoki>))
              :challenge-room (list (cons 'treasure +flamberge+)
                                    (cons 'monster '<fire-creature>)))))

