;;;; main.lisp
;;;; not sure how to explain this one, honestly

(in-package #:samovar)


;; configure blt (fonts and stuff)
(defun config ()
  (blt:set "window.title = samovar")
  (blt:set "font: fonts/unifont.ttf, size=10x16;")
  ;; (blt:set "window.resizeable = false")
  (blt:set "window.size = 100x40"))


;; TODO: come up with a better naming scheme / let the user type in
;; the name
(defun autosave ()
  "Back up the current name in a savefile."
  (let* ((path (format nil "~d.sv" (get-universal-time))))
    (save-game path)))

(defun init ()
  (setq *scene* :title)
  (clear-status)
  ;; seed random state
  (setf *random-state* (make-random-state t)))

(defun main ()
  (init)
  (blt:with-terminal
    (config)
    (loop
      (blt:refresh)
      (case *scene*
        (:play
         (when (blt:has-input-p)
           (play-input (blt:read))
           (with-tick
             (update-game)
             (collect-garbage))
           (draw-game)))
        (:title
         (draw-title)
         (blt:refresh)
         (blt:key-case
          (blt:read)
          (:escape (return))
          (:l (load-game-menu))
          (:enter (init-game)
                  (setq *scene* :play)
                  (draw-game))))
        (:win
         (draw-congratulations)
         (blt:refresh)
         (blt:key-case
          (blt:read)
          (:m (setq *scene* :title))
          (:q (return))))
        (:quit
         (draw-quit)
         (blt:refresh)
         (blt:key-case
          (blt:read)
          (:y (autosave) (setq *scene* :title))
          (:n (setq *scene* :title))
          (:c (setq *scene* :play) (draw-game))))
        (:death
         (draw-death)
         (blt:refresh)
         (blt:key-case
          (blt:read)
          (:m (setq *scene* :title))
          (:q (return)))))))
  (format t "Goodbye!~%"))
