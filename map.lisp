;;;; map.lisp
;;;; a bunch of stuff for dealing with the game map

(in-package #:samovar)

(defparameter *width* 60)
(defparameter *height* 30)

;; a space on the game map, containing a bunch
;; of miscellaneous data
(defstruct <cell>
  (memory nil)
  (visible nil)
  (tile nil)
  (monst nil)
  (items nil))


(defparameter *map* nil)
;; save stairs positions for convenience
(defparameter *upstairs-pos* nil)
(defparameter *downstairs-pos* nil)


(defclass <tile> ()
  ((walkable :initarg :walkable :accessor walkable)
   (opaque :initarg :opaque :accessor opaque)
   (ch :initarg :ch :accessor ch)
   (fg :initarg :fg :accessor fg)
   (bg :initarg :bg :accessor bg)
   (light :initarg :light :initform nil :accessor light)
   (tag :initarg :tag :accessor tag)
   (desc :initarg :desc :accessor desc)))


(defparameter +swill+
  (make-instance '<tile> :walkable t :opaque nil :ch ","
                         :fg +darkgreen+ :bg +black+
                         :tag :swill
                         :desc "A pile of suspicious leafy swill."))

(defparameter +straw+
  (make-instance '<tile> :walkable t :opaque nil :ch "刍"
                         :fg +gold+ :bg +black+
                         :tag :straw
                         :desc "A pile of damp, moldy straw."))

(defparameter +obsidian+
  (make-instance '<tile> :walkable t :opaque nil :ch "~"
                         :fg +palepurple+ :bg +black+
                         :tag :obsidian
                         :desc "Glassy stone formed from lava."))

(defparameter +melt+
  (make-instance '<tile> :walkable t :opaque nil :ch "^"
                         :fg +red+ :bg +black+
                         :tag :melt
                         :light '(300 5)
                         :desc "Hot molten stone."))

(defparameter +wood+
  (make-instance '<tile> :walkable nil :opaque t :ch "#"
                         :fg +black+ :bg +goldenrod+
                         :tag :wood
                         :desc "Wood, like from a tree."))

(defparameter +statue+
  (make-instance '<tile> :walkable nil :opaque t :ch "ß"
                         :fg +ivory+ :bg (blt:gray :value 0.5)
                         :tag :statue
                         :desc "An old statue."))

(defparameter +blood+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +white+ :bg (blt:rgba #x76 #x03 #x03)
                         :tag :blood
                         :desc "A bloodstained patch on the ground."))

(defparameter +bloody-water+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +ivory+ :bg (blt:rgba #x54 #x0b #x4a)
                         :tag :bloody-water
                         :desc "Shallow water with blood in it."))

(defparameter +frozen-blood+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +ivory+ :bg (blt:rgba #x54 #x0b #x4a)
                         :tag :frozen-blood
                         :desc "A puddle of frozen blood."))

(defparameter +mycelium+
  (make-instance '<tile> :walkable t :opaque nil :ch "\""
                         :fg (blt:rgba #xdc #xa2 #xd4) :bg +black+
                         :tag :mycelium
                         :desc "A patch of mycelium."))


(defparameter +luminescent-fungus+
  (make-instance '<tile> :walkable t :opaque nil :ch "\""
                         :fg +cyan+ :bg +black+
                         :light (list 180 5)
                         :tag :luminescent-fungus
                         :desc "A patch of luminescent fungus."))

(defparameter +spores+
  (make-instance '<tile> :walkable t :opaque nil :ch "∷"
                         :fg +cornsilk+ :bg +black+
                         :tag :spores
                         :desc "A pile of powdery mushroom spores."))


(defparameter +closed-door+
  (make-instance '<tile> :walkable t :opaque t :ch "+"
                         :fg +sienna+ :bg +black+
                         :tag :closed-door
                         :desc "A closed door."))

(defparameter +jammed-door+
  (make-instance '<tile> :walkable nil :opaque t :ch "╫"
                         :fg +sienna+ :bg +black+
                         :tag :jammed-door
                         :desc "A door that has been jammed."))

(defparameter +broken-door+
  (make-instance '<tile> :walkable t :opaque nil :ch "/"
                         :fg +sienna+ :bg +black+
                         :tag :broken-door
                         :desc "The splintered remains of a door."))

(defparameter +open-door+
  (make-instance '<tile> :walkable t :opaque nil :ch "|"
                         :fg +sienna+ :bg +black+
                         :tag :open-door
                         :desc "An open door."))

(defparameter +water+
  (make-instance '<tile> :walkable nil :opaque nil :ch "~"
                         :fg +cornflower+ :bg +ultramarine+
                         :tag :water
                         :desc "Water."))

(defparameter +shallow-water+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +sky-blue+ :bg +cornflower+
                         :tag :shallow-water
                         :desc "Shallow water."))

(defparameter +vines+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +sienna+ :bg +darkgreen+
                         :tag :vines
                         :desc "A tangle of long, grasping vines."))


(defparameter +plant+
  (make-instance '<tile> :walkable t :opaque t :ch "♈"
                         :fg +springgreen+ :bg +black+
                         :tag :plant
                         :desc "A tall plant."))

(defparameter +dead-plant+
  (make-instance '<tile> :walkable t :opaque nil :ch "\""
                         :fg (blt:rgba #x81 #x76 #x5d)
                         :bg (blt:rgba #x20 #x24 #x26)
                         :tag :dead-plant
                         :desc "The withered remains of a plant."))

(defparameter +chasm+
  (make-instance '<tile> :walkable nil :opaque nil :ch "∷"
                         :fg (blt:gray :value 0.3) :bg +black+
                         :tag :chasm
                         :desc "A dark chasm."))

(defparameter +floor+
  (make-instance '<tile> :walkable t :opaque nil :ch "."
                         :fg +white+ :bg +black+
                         :tag :floor
                         :desc "The cave floor."))

(defparameter +wall+
  (make-instance '<tile> :walkable nil :opaque t :ch "#"
                         :fg +black+ :bg +cornsilk+
                         :tag :wall
                         :desc "A wall."))

(defparameter +wall-smooth+
  (make-instance '<tile> :walkable nil :opaque t :ch "#"
                         :fg (blt:rgba #x70 #x70 #xff) :bg +black+
                         :tag :wall-smooth
                         :desc "A smooth, carved wall."))

(defparameter +wall-fungus+
  (make-instance '<tile> :walkable nil :opaque t :ch "⢖"
                         :fg +black+ :bg +gold+
                         :tag :wall-fungus
                         :desc "A wall covered in fungus."))

(defparameter +wall-lichen+
  (make-instance '<tile> :walkable nil :opaque t :ch "#"
                         :fg +black+ :bg +cyan+
                         :light '(180 5)
                         :tag :wall-lichen
                         :desc "A wall covered in lichen."))


(defparameter +wall-light+
  (make-instance '<tile> :walkable nil :opaque t :ch "#"
                         :fg +gold+ :bg +cornsilk+
                         :tag :wall-light
                         :light (list nil 10)
                         :desc "A light fixed onto the wall."))

(defparameter +downstairs+
  (make-instance '<tile> :walkable t :opaque nil :ch ">"
                         :fg +yellow+ :bg +black+
                         :light (list 60 10)
                         :tag :downstairs
                         :desc "A passage leading deeper into the cave."))

(defparameter +upstairs+
  (make-instance '<tile> :walkable t :opaque nil :ch "<"
                         :fg +yellow+ :bg +black+
                         :light (list 60 10)
                         :tag :upstairs
                         :desc "A passage leading upwards."))

(defparameter +ashes+
  (make-instance '<tile> :walkable t :opaque nil :ch "`"
                         :fg +black+ :bg (blt:gray :value 0.3)
                         :tag :ashes
                         :desc "A small pile of ashes."))

;; for debugging
(defparameter +flag+
  (make-instance '<tile> :walkable t :opaque nil :ch "%"
                         :fg +black+ :bg +pink+
                         :tag :flag
                         :desc "FLAG!!!"))


(defun place-upstairs (coord)
  "Set the position of stairs leading up."
  (destructuring-bind (x y) coord (set-tile x y +upstairs+))
  (setq *upstairs-pos* coord))

(defun place-downstairs (coord)
  "Set the position of stairs leading down."
  (destructuring-bind (x y) coord (set-tile x y +downstairs+))
  (setq *downstairs-pos* coord))



(defun init-map ()
  "Initialize the game map and fill it with wall cells."
  (setq *map* (make-array (list *width* *height*)))
  (loop for i below *width*
        do (loop for j below *height*
                 do (set-cell i j (make-<cell> :tile +wall+)))))

(defun in-bounds (map coord)
  "Determine whether `coord' is within the bounds of `map'"
  (destructuring-bind (x y) coord
    (and (>= x 0) (< x (array-dimension map 0))
         (>= y 0) (< y (array-dimension map 1)))))

(defun neighbor-coords (coord)
  "Get all locations (maximum of 8) neighboring a given tile."
  (destructuring-bind (x y) coord
    (filter (lambda (p) (in-bounds *map* p))
            (list (list (1- x) y)
                  (list (1+ x) y)
                  (list x (1- y))
                  (list x (1+ y))
                  (list (1- x) (1- y))
                  (list (1+ x) (1+ y))
                  (list (1+ x) (1- y))
                  (list (1- x) (1+ y))))))

(defun 4-neighbor-coords (coord)
  "Get all neighbors in the 4-cell neighborhood."
  (destructuring-bind (x y) coord
    (filter (lambda (p) (in-bounds *map* p))
            (list (list (1- x) y)
                  (list (1+ x) y)
                  (list x (1- y))
                  (list x (1+ y))))))


(defun remember-tile (i j)
  "Copy tile in `*map*' into `*memory*'"
  (setf (<cell>-memory (aref *map* i j)) (get-tile i j)))

(defun remembered-tile (i j)
  "Get the memory entry at `ij'"
  (<cell>-memory (get-cell i j)))


(defun set-cell (x y cell)
  (setf (aref *map* x y) cell))

(defun get-player-cell (&optional getter)
  "Get the cell that the player is standing on"
  (let ((cell (get-cell (x *player*) (y *player*))))
    (if getter (funcall getter cell) cell)))

(defun get-cell (x y &optional getter)
  (let ((cell (aref *map* x y)))
    (if getter (funcall getter cell) cell)))

(defun set-tile (x y tile)
  (setf (<cell>-tile (aref *map* x y)) tile))

(defun get-tile (x y)
  (<cell>-tile (get-cell x y)))

(defun find-walkable-place (&optional (x 0) (y 0) (w *width*) (h *height*))
  (loop for c = (list (+ 1 x (random (1- w)))
                      (+ 1 y (random (1- h))))
        until (walkable (apply #'get-tile c))
        finally (return c)))

(defun breadth-first-search (start visit-tile-p &key with-tile before-turn after-turn)
  (let ((marks (make-array (list *width* *height*) :initial-element nil))
        (q (cl-speedy-queue:make-queue (* *width* *height*))))
    (cl-speedy-queue:enqueue start q)
    (setf (aref2 marks start) t)
    (loop until (cl-speedy-queue:queue-empty-p q)
          for n from 0 do
            (let ((q* (cl-speedy-queue:make-queue (* *width* *height*))))
              ;; empty out `q' while adding to `q*'
              (when before-turn (funcall before-turn))
              (loop until (cl-speedy-queue:queue-empty-p q) do
                (let* ((c (cl-speedy-queue:dequeue q)))
                  (when with-tile (funcall with-tile c))
                  ;; enqueue unmarked, accessible neighbors
                  (loop for d in (4-neighbor-coords c)
                        when (and (not (aref2 marks d))
                                  (funcall visit-tile-p c))
                          do (cl-speedy-queue:enqueue d q*)
                             (setf (aref2 marks d) t))))
              (setf q q*)
              (when after-turn (funcall after-turn))))))

(defun line-clear-p (start end)
  "Determine whether the straight line between `start' and `end' is
  clear of obstructions."
  (let ((path (bresenham-line start end)))
    (every (lambda (c)
             (let ((tile (apply #'get-tile c)))
               (and (walkable tile) (not (opaque tile)))))
           path)))
