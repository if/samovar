;;;; monsts.lisp
;;;; definitions of and utilities for monsters

(in-package #:samovar)


(defflags %flees %wanders %blink-through %blink-towards %has-minions
  %cast-spark %cast-c-beam %keeps-distance %infernal)

(defun test-flags (m &rest flags)
  (not (zerop (apply #'logand (flags m) flags))))


(defclass <monst> ()
  ((x :initarg :x :accessor x)
   (y :initarg :y :accessor y)
   (hp :initarg :hp :accessor hp)
   (max-hp :initform 0 :accessor max-hp)
   (ch :initform "X" :accessor ch)
   (name :initform "Generic monster" :accessor name)
   (desc :initform "A generic monster." :accessor desc)
   ;; list of verbs describing how the monster attacks
   ;; for example ("slashes" "lacerates")
   (verbs :initform '("attacks") :accessor verbs)
   (color :initform +white+ :accessor color)
   (light :initform nil :accessor light)
   (state :initform :sleep :accessor state)
   (flags :initform 0 :accessor flags)
   ;; melee damage
   (dmg :initform 0 :accessor dmg)
   ;; defense  (0.0 - 1.0)
   (df  :initform 0.0 :accessor df)
   (regen :initform 0.0 :accessor regen)
   ;; tracks all the active effects
   (effects :initform (make-hash-table) :accessor effects)
   ;; the poison (= magical effect) that the monster carries
   (poison :initform nil :accessor poison)
   ;; direction of wandering
   (bearing :initform :n :accessor bearing)
   ;; item dropped upon death
   (drops :initform nil :accessor drops)))

;; kludge
(defmethod calc-damage ((m <monst>))
  "Calculate the damage a monster can inflict, based on its equipment
  and magic."
  (dmg m))


(defun has-effect (e m)
  "Determine whether effect `e' is currently acting on monster `m'."
  (gethash e (effects m)))

;; splits the monster apart into 'component' monsters, if it has any.
(defmethod analyze ((m <monst>)))

;; a helper for building analysis methods; most symbiotic monsters can
;; use this.
(defun quick-analyze (m classes)
  (loop for c in (neighbor-coords (monst-coord m))
        when (and classes (walkable (apply #'get-tile c)))
          do (spawn-monst (pop classes) :coord c :state :hunt))
  ;; kill m
  (cleanup-monst m))


(defmethod pick-verb ((m <monst>))
  (nth (random (length (verbs m))) (verbs m)))

(defmethod monst-coord ((m <monst>))
  "Get the monster's location as an ordered pair (list) for
  convenience."
  (list (x m) (y m)))


(defmethod monst-heal ((m <monst>))
  "Regenerate health."
  (when (< (hp m) (max-hp m))
    (setf (hp m) (min (+ (hp m) (regen m)) (max-hp m)))))

(defun start-effect (e m)
  (case e
    (:health (setf (hp m) (min (max-hp m) (+ 10 (hp m)))))
    (:harm (setf (hp m) (max 0 (- (hp m) 10))))
    (:cure (setf (effects m) (make-hash-table)))
    (:swill
     (loop for (i j) in (neighbor-coords (monst-coord m))
           when (and (walkable (get-tile i j))
                     (< (random 1.0) 0.7))
             do (set-tile i j +swill+)))))

(defun end-effect (e m)
  (case e
    (:chifir (setf (hp m) 0))
    (:invisibility (setf (stealth m) nil))))

(defun run-effect (effect m)
  "Give one turn's worth of a magical effect to `monst'."
  (case effect
    (:poison (setf (hp m) (max (- (hp m) 0.5) 0)))
    (:chifir (setf (hp m) 1) (setf (df m) 1.0))
    (:invisibility (setf (stealth m) t))))

(defmethod affect-monst ((m <monst>))
  "Run a monster's magical effects."
  (loop for k being the hash-keys of (effects m)
          using (hash-value v)
        when v do
          (if (<= v 0.0)
              (progn (setf (gethash k (effects m)) nil)
                     (end-effect k m))
              (progn (run-effect k m)
                     (decf (gethash k (effects m)))))))

(defclass <player> (<monst>)
  ((max-hp :initform 20)
   (regen :initform 0.2)
   (ch :initform "@")
   (name :initform "You")
   (desc :initform "Something more complex and intelligent than any monster.")
   (verbs :initform '("hit" "attack"))
   (color :initform +yellow+)
   (df  :initform 0.0)
   (dmg :initform   2)
   ;; df and dmg when player has no equipment
   (base-df  :initform 0.0 :accessor base-df)
   (base-dmg :initform   2 :accessor base-dmg)
   (armor  :initform nil :accessor armor)
   (weapon :initform nil :accessor weapon)
   ;; later on, this could be a value between 0.0 and 1.0
   (stealth :initform nil :accessor stealth)))


;; kind of a kludge
(defun update-equipment (p)
  "Update the player's stats based on the equipment he has."
  (if (weapon p)
      (setf (dmg p) (dmg (weapon p)))
      (setf (dmg p) (base-dmg p)))
  (if (armor p)
      (setf (df p) (df (armor p)))
      (setf (df p) (base-df p))))


(defmethod update-monst ((m <player>))
  (affect-monst m))

(defun drink-tea (tea)
  "Drink tea, and apply the tea's effects to the player."
  (apply-tea-effect tea *player*)
  (set-status (format nil "[color=amber]~a[/color]"
                      (case (effect tea)
                        (:health "You feel healthier.")
                        (:invisibility "You feel stealthy.")
                        (:cure "You feel better.")
                        (:chifir "You feel intoxicated!")
                        (:nothing "You feel hydrated.")
                        (:harm "Everything hurts!!!")
                        (:swill "You vomit everywhere.")
                        (:poison "You feel ill.")))))




(defclass <spaghetti-worm> (<monst>)
  ((max-hp :initform 10)
   (ch :initform "l")
   (name :initform "Spaghetti worm")
   (desc :initform "An agile and highly elastic worm.")
   (verbs :initform '("pierces" "whips"))
   (color :initform +yellow+)
   (df :initform 0.2)
   (flags :initform %blink-through)
   (dmg :initform 5)))

(defclass <cricket-spider> (<monst>)
  ((max-hp :initform 5)
   (ch :initform "s")
   (name :initform "Cricket spider" :accessor name)
   (desc :initform "A small spider that looks like a cricket.")
   (verbs :initform '("bites" "nips"))
   (color :initform +dark-khaki+)
   (df  :initform 0.0 :accessor df)
   (dmg :initform   3 :accessor dmg)
   (flags :initform %wanders)))

(defclass <spider-cricket> (<monst>)
  ((max-hp :initform 7)
   (ch :initform "c")
   (name :initform "Spider cricket" :accessor name)
   (desc :initform "A cricket that looks like a spider.")
   (verbs :initform '("chews" "nips"))
   (color :initform +dark-khaki+)
   (df  :initform 0.2 :accessor df)
   (dmg :initform   5 :accessor dmg)
   (flags :initform (logior %blink-towards %wanders))))


(defclass <white-mushroom> (<monst>)
  ((max-hp :initform 10)
   (ch :initform "m")
   (name :initform "White mushroom")
   (desc :initform "An edible mushroom, also known as the 'champignon'.")
   (verbs :initform '("vomits on" "kisses" "headbutts"))
   (color :initform +ivory+)
   (df  :initform 0.1)
   (flags :initform %flees)
   (dmg :initform   3)
   (regen :initform 0.5)))

(defclass <straw-mushroom> (<monst>)
  ((max-hp :initform 10)
   (ch :initform "n")
   (name :initform "Straw mushroom")
   (desc :initform "An small but tough mushroom, easily mistaken for the death cap.")
   (verbs :initform '("licks" "squishes"))
   (color :initform +pink+)
   (df  :initform 0.5)
   (flags :initform (logior %wanders %flees))
   (dmg :initform 4)))

(defclass <death-cap> (<monst>)
  ((max-hp :initform 13)
   (ch :initform "n")
   (name :initform "Straw mushroom")
   (desc :initform "Extremely deadly, easily mistaken for the straw mushroom.")
   (verbs :initform '("chomps" "nibbles"))
   (color :initform +cornsilk+)
   (df  :initform 0.1)
   (dmg :initform 4)
   (poison :initform :poison)
   (regent :initform 4)))

(defclass <wood-ear> (<monst>)
  ((max-hp :initform 40)
   (ch :initform "W")
   (name :initform "Wood ear")
   (desc :initform "A huge but relatively gentle mushroom.")
   (verbs :initform '("shoves" "pushes" "clouts"))
   (color :initform +dark-khaki+)
   (df  :initform 0.7)
   (dmg :initform 1)
   (regen :initform 2)))

(defclass <fire-creature> (<monst>)
  ((max-hp :initform 50)
   (ch :initform "F")
   (name :initform "Fire creature")
   (desc :initform "A mysterious creature covered in white-hot plate armor.")
   (verbs :initform '("burns" "maims"))
   (color :initform +orange+)
   (df  :initform 0.5)
   (light :initform '(0 10))
   (flags :initform %infernal)
   (dmg :initform   20)
   (drops :initform +mellifluous-jasmine+)))

(defclass <gardener> (<monst>)
  ((max-hp :initform 20)
   (ch :initform "G")
   (name :initform "Gardener")
   (desc :initform "A fungus that grows other fungi.")
   (verbs :initform '("stabs" "slashes"))
   (color :initform +springgreen+)
   (dmg :initform 5)
   (flags :initform (logior %wanders %keeps-distance %has-minions))
   (regen :initform 10)))

(defclass <bleeding-tooth> (<monst>)
  ((max-hp :initform 3)
   (ch :initform "u")
   (name :initform "Bleeding tooth")
   (desc :initform "A pale, fleshy mushroom full of blood")
   (verbs :initform '("suckers" "scratches"))
   (color :initform +pink+)
   (flags :initform %wanders)
   (dmg :initform 2)
   (regen :initform 10)))

(defclass <amanita> (<monst>)
  ((max-hp :initform 20)
   (ch :initform "m")
   (name :initform "Amanita")
   (desc :initform "A squishy, unusually poisonous mushroom.")
   (verbs :initform '("spits on" "mashes"))
   (color :initform +orange+)
   (df  :initform 0.0)
   (dmg :initform   3)
   (regen :initform 0.5)
   (poison :initform :poison)
   (flags :initform (logior %flees %wanders))))

(defclass <red-mushroom> (<monst>)
  ((max-hp :initform 10)
   (ch :initform "r")
   (name :initform "Red mushroom")
   (desc :initform "A small flimsy fungus that sustains itself on blood.")
   (verbs :initform '("bites"))
   (color :initform +red+)
   (df  :initform 0.0)
   (dmg :initform   3)
   (regen :initform 0.5)))


(defclass <enoki> (<monst>)
  ((max-hp :initform 3)
   (ch :initform "e")
   (name :initform "Enoki")
   (desc :initform "A small, rather cute mushroom.")
   (verbs :initform '("nips"))
   (color :initform +cornsilk+)
   (df  :initform 0.0)
   (dmg :initform   5)
   (flags :initform %wanders)))

(defmethod analyze ((m <enoki>))
  (quick-analyze m '(<enoki> <enoki> <enoki> <enoki> <enoki> <enoki>
                     <enoki> <enoki>)))

(defclass <greater-enoki> (<monst>)
  ((max-hp :initform 27)
   (ch :initform "E")
   (name :initform "Greater enoki")
   (desc :initform "A large, compound version of the enoki mushroom.")
   (verbs :initform '("whisks"))
   (color :initform +cornsilk+)
   (df  :initform 0.2)
   (dmg :initform   15)
   (regen :initform 0.2)))

(defmethod analyze ((m <greater-enoki>))
  (quick-analyze m '(<greater-enoki> <enoki> <enoki> <enoki> <enoki> <enoki>
                     <enoki> <enoki>)))

(defclass <glow-worm> (<monst>)
  ((max-hp :initform 15)
   (ch :initform "w")
   (name :initform "Glow worm")
   (desc :initform "A glowing worm, adapted to live in dark and damp caves.")
   (verbs :initform '("chomps on"))
   (color :initform +palepurple+)
   (light :initform '(160 10))
   (df  :initform 0.0 :accessor df)
   (dmg :initform   4 :accessor dmg)
   (flags :initform %wanders)))

(defclass <redcap> (<monst>)
  ((max-hp :initform 25)
   (ch :initform "R")
   (name :initform "Redcap")
   (desc :initform "A massive, crimson-headed fungus that thrives on blood.")
   (verbs :initform '("bites" "slices" "bludgeons"))
   (color :initform +red+)
   (dmg :initform   7)
   (regen :initform 1)
   (flags :initform %wanders)))

(defmethod analyze ((m <redcap>))
  (quick-analyze m '(<red-mushroom> <red-mushroom> <white-mushroom>)))

(defclass <porcelain-creature> (<monst>)
  ((max-hp :initform 10)
   (ch :initform "P")
   (name :initform "Porcelain creature")
   (desc :initform "A mysterious being, clothed in porcelain.")
   (verbs :initform '("slices" "lacerates"))
   (color :initform +porcelain+)
   (df  :initform 0.4)
   (dmg :initform   10)
   (flags :initform (logior %cast-spark %keeps-distance))
   (regen :initform 1)
   (drops :initform +card-spark+)))

(defclass <lace-creature> (<monst>)
  ((max-hp :initform 15)
   (ch :initform "L")
   (name :initform "Lace creature")
   (desc :initform "A mysterious being, clothed in porcelain.")
   (verbs :initform '("tickles"))
   (color :initform +gold+)
   (dmg :initform   10)
   (flags :initform (logior %cast-c-beam %keeps-distance %wanders))
   (regen :initform 1)
   (drops :initform +card-c-beam+)))





(defun drip-blood (i j)
  "Drip blood on a certain tile, possibly modifying the tile or
  triggering other events"
  (let ((tl (get-tile i j)))
    (when (and (eq (tag tl) :mycelium)
               (< (random 1.0) 0.7)
               (not (get-cell i j #'<cell>-monst)))
      (spawn-monst '<redcap> :coord (list i j) :state :hunt)
      (set-status "A redcap rises up from the bloodstained ground!"))
    (case (tag tl)
      (:floor (set-tile i j +blood+))
      (:shallow-water (set-tile i j +bloody-water+))
      (:luminescent-fungus (set-tile i j +dead-plant+))
      (:mycelium (set-tile i j +blood+)))))

(defmethod bleed ((m <monst>))
  (let ((coords (cons (monst-coord m)
                      (neighbor-coords (monst-coord m)))))
    (loop for (i j) in coords
          for tl = (get-tile i j)
          when (and (walkable tl) (not (opaque tl))
                    (< (random 1.0) 0.1))
            do (drip-blood i j))))

(defmethod bleed ((m <bleeding-tooth>))
  (loop for dc in +circle-3+ do
    (let ((c (add-coords dc (monst-coord m))))
      (when (and (in-bounds *map* c)
                 (< (random 1.0) 0.7))
        (apply #'drip-blood c)))))

;; redcaps don't bleed
(defmethod bleed ((m <redcap>)) (declare (ignore m)))

(defun damage-monst (m d &optional always-accurate (acc-bonus 0))
  "Try to damage a monster `m', missing if the damage cannot get past
the monster's defense.  Returns whether the damage hit the monster."
  (let ((hits (> (random 1.0) (- (df m) acc-bonus))))
    (when (or always-accurate hits)
      (setf (hp m) (- (hp m) d))
      (bleed m))
    hits))

(defmethod attack ((m1 <player>) (m2 <monst>))
  "`m1' attacks `m2'"
  ;; the player can make critical hits (1.5x damage, always accurate)
  ;; if the monster is caught by surprise
  (let* ((s (state m2))
         (crit-p (or (eq s :wander) (eq s :sleep)))
         (mult (if crit-p 1.5 1.0)))
    (if (damage-monst m2 (* mult (calc-damage m1)) crit-p
                      (if (weapon m1)
                          (acc-bonus (weapon m1))
                          0))
        (progn (set-status (format nil "You ~a the ~a!" (pick-verb m1) (name m2)))
               (when crit-p (set-status "[color=green]Critical hit![/color]")))
        (set-status (format nil "You miss the ~a." (name m2))))))

(defmethod attack ((m1 <monst>) (m2 <player>))
  "`m1' attacks `m2'"
  (if (damage-monst m2 (calc-damage m1))
      (progn
        (when (poison m1) (apply-effect (poison m1) 2 m2))
        (set-status (format nil "The ~a ~a you!"
                            (name m1) (pick-verb m1))))
      (set-status (format nil "The ~a tries to attack you, but misses."
                          (name m1)))))


(defmethod attack ((m1 <monst>) (m2 <monst>))
  "`m1' attacks `m2'"
  (if (damage-monst m2 (calc-damage m1))
      (progn
        (when (poison m1) (apply-effect (poison m1) 2 m2))
        (set-status (format nil "The ~a ~a the ~a!"
                            (name m1) (pick-verb m1) (name m2))))
      (set-status (format nil "The ~a misses the ~a."
                          (name m1) (name m2)))))


(defun blink-attack (monst target)
  "Attack a monster by blinking through it"
  (set-status (format nil "The ~a blinks!" (name monst)))
  (launch-ray (make-<bolt> :start (monst-coord monst)
                           :end (monst-coord target)
                           :animate (blink-monst monst target t))))

(defun blink-towards (monst target)
  "Blink towards a target monster"
  (set-status (format nil "The ~a blinks!" (name monst)))
  (launch-bolt (make-<bolt> :start (monst-coord monst)
                            :end (monst-coord target)
                            :animate (blink-monst monst target))))


(defun cleanup-monst (m)
  "Remove a monster from the game."
  (unplace-monst m)
  (set-status (format nil "The ~a dies." (name m))))

(defmethod can-walk ((m <monst>))
  "Create a function for determining whether a cell can be walked on."
  (lambda (coord)
    (let* ((c (apply #'get-cell coord))
           (tile (<cell>-tile c))
           (m2 (<cell>-monst c)))
      (and (walkable tile)
           (or (not m2) (eql m2 m) (eql m2 *player*))))))

(defmethod summon-minions ((m <monst>))
  (set-status "Spores fly out of the gardener's cap!")
  (loop for c in (neighbor-coords (monst-coord m))
        when (and (< (random 1.0) 0.4)
                  (walkable (apply #'get-tile c)))
          do (let ((type (pick-random '(<bleeding-tooth> <red-mushroom>))))
               (spawn-monst type :coord c :state :hunt))))


(defmethod update-monst ((m <monst>))
  (cond
    ;; if a monster is dead, set the flag
    ;; it should be cleaned up in the next tick
    ((monst-dead-p m)
     (wlet ((it (drops m)))
       (apply #'place-item it (monst-coord m)))
     (setf (state m) :dead))
    ;; if fleeing, go to sleep with a low probability
    ((and (eq (state m) :flee)
          (< (random 1.0) 0.1))
     (monst-heal m)
     (setf (state m) :sleep))
    ;; flee if health is low
    ((and (test-flags m %flees) (monst-low-health-p m))
     (monst-flee m)
     (setf (state m) :flee))
    ;; attack if next to player
    ((monst-adjacent-player-p m)
     (monst-attack-player m)
     (setf (state m) :attack))
    ;; attack by blinking through player
    ((and (test-flags m %blink-through)
          (< (random 1.0) 0.4)
          (eq (state m) :hunt)
          (monst-close-to-player-p m))
     (blink-attack m *player*)
     (setf (state m) :attack))
    ;; blink towards a player
    ((and (test-flags m %blink-towards)
          (< (random 1.0) 0.4)
          (eq (state m) :hunt)
          (monst-close-to-player-p m))
     (blink-towards m *player*)
     (setf (state m) :attack))
    ;; cast spark
    ((and (test-flags m %cast-spark)
          (< (random 1.0) 0.4)
          (eq (state m) :hunt)
          (line-clear-p (monst-coord m)
                        (monst-coord *player*)))
     (set-status (format nil "The ~a shoots out a spark!" (name m)))
     (cast-spark (monst-coord m) (monst-coord *player*) 10))
    ;; cast c-beam
    ((and (test-flags m %cast-c-beam)
          (< (random 1.0) 0.4)
          (eq (state m) :hunt)
          (line-clear-p (monst-coord m)
                        (monst-coord *player*)))
     (set-status (format nil "The ~a shoots out a C-beam!" (name m)))
     (cast-c-beam (monst-coord m) (monst-coord *player*) 20))
    ;; summon minions
    ((and (test-flags m %has-minions)
          (< (random 1.0) 0.3)
          (eq (state m) :hunt))
     (summon-minions m))
    ;; hunt player
    ((monst-sees-player-p m)
     (monst-pursue-player m)
     (setf (state m) :hunt))
    ;; wander around aimlessly
    ((test-flags m %wanders)
     (monst-wander m)
     (setf (state m) :wander))
    ;; sleep and heal
    (t (monst-heal m)
       (setf (state m) :sleep))))


;; the greater enoki splits into small enokis when killed
(defmethod update-monst ((m <greater-enoki>))
  (call-next-method)
  (when (eq :dead (state m))
    (quick-analyze m '(<enoki> <enoki> <enoki> <enoki> <enoki>
                       <enoki> <enoki> <enoki>))))

(defun drink-blood (m)
  (when (eq :blood (tag (apply #'get-tile (monst-coord m))))
    (monst-heal m)
    (set-tile (x m) (y m) +floor+)))

(defmethod update-monst ((m <redcap>))
  (call-next-method)
  (drink-blood m))

(defmethod update-monst ((m <gardener>))
  (call-next-method)
  (drink-blood m))


(defmethod update-monst ((m <bleeding-tooth>))
  (call-next-method)
  (drink-blood m))

(defmethod update-monst ((m <red-mushroom>))
  (call-next-method)
  (drink-blood m))

(defmethod update-monst ((m <fire-creature>))
  (call-next-method)
  (loop for (i j) in (neighbor-coords (monst-coord m))
        when (< (random 1.0) 0.2) do
          (set-tile i j +obsidian+)
        else when (< (random 1.0) 0.2) do
          (set-tile i j +melt+)))


(defun monst-navigate (m field)
  "Move a monster along a flow field.  Return the cost at the
monster's new position.  The cost may be `nil' if the monster has
nowhere to go."
  (let ((c (flow-to-tile (monst-coord m) field)))
    (apply #'place-monst m c))
  (cost-at (monst-coord m) field))


(defun flow-to-tile (from field)
  "Return the lowest-cost neighbor of `from'."
  (let* ((nbs (neighbor-coords from))
         (next from)
         (next-cost (cost-at from field)))
    (loop for coord in nbs
          for c = (cost-at coord field)
          ;; VERY IMPORTANT: using <= means that monsters will
          ;; anxiously pace around when cornered, which makes their
          ;; fleeing behavior seem more intelligent.
          when (and c next-cost (<= c next-cost))
            do (setf next coord)
               (setf next-cost c))
    next))



(defun new-monst (type &optional (x 0) (y 0))
  "Create a new monster of class `type'"
  (let ((m (make-instance type :x x :y y)))
    (setf (hp m) (max-hp m))
    m))

(defun spawn-monst (type &key coord state)
  "Create a new monster and place it on the current level."
  (if (listp type)
      (spawn-horde type :coord coord :state state)
      (let ((m (new-monst type (first coord) (second coord))))
        (setf (state m) state)
        (apply #'place-monst m coord)
        (push m *monsts*))))

(defun spawn-horde (types &key coord state)
  "Spawn a horde of monsters, specified by `types',
around a specific coordinate and starting in a specific state."
  (let ((r 1))
    (loop while (and (< r 20) types) do
      (let ((c (add-coords (list (- (random (* 2 r)) r)
                                 (- (random (* 2 r)) r))
                           coord)))
        (if (and (in-bounds *map* c)
                 (walkable (apply #'get-tile c))
                 (not (<cell>-monst (apply #'get-cell c))))
            (spawn-monst (pop types) :coord c :state state)
            (incf r))))))


(defmethod unplace-monst ((m <monst>))
  "Remove a monster from the map."
  (setf (<cell>-monst (aref *map* (x m) (y m))) nil))

(defmethod place-monst ((m <monst>) xx yy)
  (unless (get-cell xx yy #'<cell>-monst)
    (setf (<cell>-monst (get-cell (x m) (y m))) nil)
    (setf (<cell>-monst  (get-cell xx yy)) m)
    (setf (x m) xx)
    (setf (y m) yy)
    (walk-hook m (get-tile xx yy))))



(defmethod move-monst ((m <monst>) dx dy)
  (unless (has-effect :paralysis m)
    (let ((xx (+ (x m) dx))
          (yy (+ (y m) dy)))
      (when (and (in-bounds *map* (list xx yy))
                 (walkable (get-tile xx yy)))
        (place-monst m xx yy)))))

(defmethod move-monst ((m <player>) dx dy)
  (if (has-effect :paralysis m)
      (set-status "You can't move!")
      (let ((xx (+ (x m) dx))
            (yy (+ (y m) dy)))
        ;; If player moves towards a monster, attack the
        ;; monster but don't update the player's position.
        (let ((attackp (weapon-attack (weapon m)
                                      m
                                      (list xx yy)
                                      (list dx dy))))
          (when (and (in-bounds *map* (list xx yy))
                     (walkable (get-tile xx yy))
                     (walkable (get-tile xx (y m)))
                     (walkable (get-tile (x m) yy))
                     (not attackp))
            (place-monst m xx yy)))
        ;; if player is standing next to a wall, give it a stealth
        ;; boost
        (if (filter (lambda (c) (let ((tg (tag (apply #'get-tile c))))
                                  (or (eq tg :wall)
                                      (eq tg :wall-lichen)
                                      (eq tg :wall-fungus))))
                    (4-neighbor-coords (monst-coord m)))
            (progn (setf (stealth m) t)
                   (setf (color m) +cornflower+))
            (progn (setf (stealth m) nil)
                   (setf (color m) +yellow+))))))


(defmethod walk-hook ((m <monst>) tile)
  "Code to run when a monsters walks onto a tile."
  (case (tag tile)
    (:closed-door (set-tile (x m) (y m) +open-door+))
    (:melt (damage-monst m 15))
    (:swill (damage-monst m 2))))

(defmethod walk-hook ((m <fire-creature>) tile)
  (case (tag tile)
    (:open-door (set-tile (x m) (y m) +broken-door+))
    (:closed-door (set-tile (x m) (y m) +broken-door+))))




(defparameter *player* (new-monst '<player>))
(defparameter *monsts* nil
  "Holds all monsters besides the player.")

