;;;; package.lisp

(defpackage #:samovar
  (:use #:cl)
  (:import-from #:alexandria #:curry))
