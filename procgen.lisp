;;;; procgen.lisp
;;;; procedural generation stuff ... a mess

(in-package #:samovar)




(defun generate-level
    (&key challenge-room
       (fungus 0.0)
       (wall-fungus 0.0)
       (mycelium 0.0)
       (wall-lichen 0.0)
       (plants 0.0)
       (straw 0.0)
       (lights 0.0)
       (monsts '(0 nil)))
  ;; get rid of monsters
  (setq *monsts* nil)
  ;; bake statue rings into the bedrock
  (place-statuary 0.2)

  ;; accrue rooms, and then turn the last room (a leaf) into a
  ;; challenge room, if the caller asked for one
  (let* ((rooms (accrue-rooms))
         (leaf (car rooms))
         (door (car (<room>-used-anchors leaf)))
         (lx (<room>-x leaf))
         (ly (<room>-y leaf))
         (lw (<room>-width leaf))
         (lh (<room>-height leaf)))
    (when challenge-room
      (loop for i below lw do
        (set-tile (+ i lx) ly +wall-smooth+)
        (set-tile (+ i lx) (+ -1 ly lh) +wall-smooth+))
      (loop for i below lh do
        (set-tile lx (+ i ly) +wall-smooth+)
        (set-tile (+ -1 lx lw) (+ i ly) +wall-smooth+))
      (set-tile (first door) (second door) +jammed-door+)
      ;; place treasure
      (apply #'place-item (cdr (assoc 'treasure challenge-room))
             (find-walkable-place lx ly lw lh))
      ;; spawn monster
      (spawn-monst (cdr (assoc 'monster challenge-room))
                   :state :sleep
                   :coord (find-walkable-place lx ly lw lh))))
  
  (flet ((on-floor (ret)
           (lambda (tile) (when (eq :floor (tag tile)) ret)))
         (on-wall (ret)
           (lambda (tile) (when (eq :wall (tag tile)) ret))))
    (noise-splash fungus (on-floor +luminescent-fungus+))
    (noise-splash plants (on-floor +plant+))
    (noise-splash straw (on-floor +straw+))
    (noise-splash mycelium (on-floor +mycelium+))
    (noise-splash wall-lichen (on-wall +wall-lichen+))
    (noise-splash wall-fungus (on-wall +wall-fungus+)))

  (loop repeat 2 do
    (let* ((l (make-lake (+ 10 (random 20)) (+ 10 (random 20))))
           (field (<room>-contents l))
           (w (<room>-width l))
           (h (<room>-height l))
           (x (<room>-x l))
           (y (<room>-y l)))
      ;; if lake fits, carve it into the map
      (when (level-connected-p
             (lambda (c tile)
               (and (walkable tile)
                    (if (and (>= (first c) x) (< (first c) (+ x w))
                             (>= (second c) y) (< (second c) (+ y h)))
                        (not (aref2 field (sub-coords c (list x y))))
                        t))))
        (loop for i from 1 below (1- w) do
          (loop for j from 1 below (1- h) do
            (when (aref field i j)
              (set-tile (+ x i) (+ y j) +water+)))))))

  ;; put a border around the part of lakes adjacent to walkable tiles
  (let ((lake-border nil))
    (loop for i below *width* do
      (loop for j below *height*
            when (walkable (get-tile i j)) do
              (loop for c in (neighbor-coords (list i j))
                    when (eq :water (tag (apply #'get-tile c))) do
                      (push c lake-border))))
    (loop for (i j) in lake-border do
      (set-tile i j +shallow-water+)))


  ;; dig tunnels and place lights
  (let ((borders (get-border-walls)))
    (alexandria:shuffle borders)
    (place-lights lights borders)
    (dig-tunnels 20 borders))

  
  ;; get rid of erroneous doors
  (erode-doors)

  ;; monsters
  (let* ((num-monsts (first monsts))
         (monst-types (second monsts))
         (ntypes (length monst-types)))
    (loop repeat num-monsts do
      (let ((type (nth (random ntypes) monst-types)))
        (spawn-monst type :coord (find-walkable-place)
                          :state :sleep))))
  
  ;; items
  (let* ((n (random 5)))
    (loop repeat n
          for c = (find-walkable-place)
          for %l = (+ 0.001 (/ (float *level*) *numlevels*))
          for it = (pick-random
                    (list (make-instance '<javelin>)
                          (make-random-armor (* 0.2 %l) (* 0.8 %l))
                          (make-random-weapon (1+ (round (* 10 %l)))
                                              (+ 2 (round (* 25 %l)))
                                              (random 1.0))))
          do (apply #'place-item it c)))
  (let* ((n (random 7))
         (leaves (list +genmaicha+ +sencha+ +oolong-tea+))
         (nleaves (length leaves)))
    (loop repeat n
          for l = (nth (random nleaves) leaves)
          for c = (find-walkable-place)
          do (place-item l (first c) (second c))))
  (let* ((n (random 7))
         (cards (list +card-teleportation+ +card-purification+
                      +card-magic-mapping+ +card-analyze+ +card-blink+))
         (ncards (length cards)))
    (loop repeat n
          for l = (nth (random ncards) cards)
          for c = (find-walkable-place)
          do (place-item l (first c) (second c)))))


(defun level-connected-p (&optional (count-tile-p
                                     (lambda (c tl)
                                       (declare (ignore c))
                                       (walkable tl))))
  "Determine whether all walkable tiles in the dungeon are connected
  together."
  (let ((total-walkables 0)
        (bfs-walkables 0)
        (start                          ; TODO: make this more concise
          (loop named outer for i below *width* do
            (loop for j below *height*
                  when (funcall count-tile-p (list i j) (get-tile i j))
                    do (return-from outer (list i j))))))
    ;; count how many tiles are accessible via bread-first-search,
    ;; starting from an arbitrary walkable tile
    (breadth-first-search
     start
     (lambda (c) (funcall count-tile-p c (apply #'get-tile c)))
     :with-tile (lambda (c)
                  (when (funcall count-tile-p c (apply #'get-tile c))
                    (incf bfs-walkables))))
    ;; count how many walkable tiles there are overall
    (loop for i below *width* do
      (loop for j below *height*
            when (funcall count-tile-p (list i j) (get-tile i j)) do
              (incf total-walkables)))
    (= total-walkables bfs-walkables)))

(defstruct <room>
  (x 0 :type integer)
  (y 0 :type integer)
  (width 0 :type integer)
  (height 0 :type integer)
  (anchors nil)
  (used-anchors nil)
  (contents))


(defun noise-splash (q callback)
  "Use noise to paint splashes of `tile' onto the map."
  (let ((di (random *width*))
        (dj (random *height*)))
    (loop for i below *width* do
      (loop for j below *height*
            ;; noise value is a float between 0 and 1
            for n = (/ (+ 1.0
                          (black-tie:simplex-noise-2d-sf
                           (/ (mod (+ i di) *width*)
                              (float *width*))
                           (/ (mod (+ j dj) *height*)
                              (float *height*))))
                       2.0)
            for tl = (get-tile i j)
            for tg = (tag tl)
            when (< n q) do
              (wlet ((place-me (funcall callback tl)))
                (set-tile i j place-me))))))


(defun make-easy-prefab (i j tiles)
  "A very convenient way of making prefabs."
  (loop for row in tiles
        for dj from 0 do
          (loop for tl in row
                for di from 0
                for x = (+ i di)
                for y = (+ j dj)
                when (and tl (in-bounds *map* (list x y))) do
                  (set-tile x y tl))))




(defun get-border-walls ()
  "Return a list of locations of walls that border walkable tiles."
  (let ((coords nil))
    (loop for i below *width* do
      (loop for j below *height*
            for tg = (tag (get-tile i j))
            when (and (eq tg :wall)
                      (some (lambda (c) (and c (walkable (<cell>-tile c))))
                            (neighbors *map* i j t)))
              do (push (list i j) coords)))
    coords))






(defun statue-ring ()
  "A prefab spec for a randomized ring of statues"
  (let ((spec          
          (list (list nil nil nil nil nil nil nil nil nil)
                (list nil nil nil :hi :hi :hi nil nil nil)
                (list nil nil :hi nil nil nil :hi nil nil)
                (list nil :hi nil nil nil nil nil :hi nil)
                (list nil :hi nil nil nil nil nil :hi nil)
                (list nil :hi nil nil nil nil nil :hi nil)
                (list nil nil :hi nil nil nil :hi nil nil)
                (list nil nil nil :hi :hi :hi nil nil nil)
                (list nil nil nil nil nil nil nil nil nil))))
    (loop for row in spec
          collect (loop for x in row
                        collect (if (and (eq x :hi) (< (random 1.0) 0.3))
                                    +statue+
                                    nil)))))


(defun place-statuary (p)
  (loop for i by 9 below *width* do
    (loop for j by 9 below *height*
          when (< (random 1.0) p)
            do (make-easy-prefab i j (statue-ring)))))


(defun place-lights (p coords)
  ;; search through walls that border non-walls
  ;; randomly replace the wall with a "light" wall
  (loop for (i j) in coords
        when (< (random 1.0) p) do
          (set-tile i j +wall-light+)))


(defun neighbors (arr i j &optional small)
  "Get a list of the 8 neighbors of (i j) in `arr'
(or 4 neighbors if `small' is set).  Cells outside the map will be
`nil'."
  (flet ((cell-at (dc)
           (destructuring-bind (di dj) dc
             (let ((i* (+ i di))
                   (j* (+ j dj)))
               (if (in-bounds arr (list i* j*))
                   (aref arr i* j*)
                   nil)))))
    (mapcar #'cell-at
            (if small
                '((0 -1) (-1 0) (1 0) (0 1))
                '((-1 -1) (0 -1) (1 -1) (-1 0)
                  (1 0) (-1 1) (0 1) (1 1))))))

(defun match-nbs (i j &rest specs)
  "Determine whether the neighbors of tile `(i j)' have
tags matching any one of `specs'."
  (let ((nbs (neighbors *map* i j)))
    (loop for spec in specs
          when (loop for nb in nbs
                     for n from 0
                     for mt = (when nb (tag (<cell>-tile nb)))
                     for st = (nth n spec)
                     unless (or (not st) (eq mt st))
                       do (return nil)
                     finally (return t))
            do (return t)
          finally (return nil))))


(defun erode-doors ()
  "Remove doors that aren't attached to at least two walls."
  (loop for i below *width* do
    (loop for j below *height*
          for tl = (get-tile i j)
          when (and (eq :closed-door (tag tl))
                    (not (match-nbs i j
                                    '(nil :wall nil
                                      nil       nil
                                      nil :wall nil)
                                    '(nil nil nil
                                      :wall :wall
                                      nil nil nil))))
            do (set-tile i j +floor+))))


(defun field-neighbors (field i j)
  "Count the number of neighbors in a lake growth field."
  (flet ((count-cell (acc nb) (if nb (1+ acc) acc)))
    (reduce #'count-cell (neighbors field i j) :initial-value 0)))

(defun do-rule (field i j)
  (let ((self (aref field i j))
        (nbs  (field-neighbors field i j)))
    ;; (or (and (not self) (>= nbs 5) (<= nbs 8))
    ;;     (and self (>= nbs 4) (<= nbs 8)))
    (or (and (not self) (>= nbs 3))
        (and self (>= nbs 5)))))


(defun make-lake (w h)
  (let* ((x (1+ (random (- *width* w))))
         (y (1+ (random (- *height* h))))
         (field (make-array (list w h) :initial-element nil)))
    ;; seed field
    (loop for i from 1 below (1- w) do
      (loop for j from 1 below (1- h) do
        (setf (aref field i j) (< (random 1.0) 0.45))))
    ;; iterate update rule 5x
    (loop repeat 5 do
      (loop for i from 1 below (1- w) do
        (loop for j from 1 below  (1- h) do
          (setf (aref field i j) (do-rule field i j)))))
    (make-<room> :x x :y y :contents field :width w :height h)))


;; rectangle rooms, blob rooms, rooms with hallway anchor
(defun design-rect-room ()
  "Make a simple rectangular room design."
  (let* ((w (+ 5 (random 7)))
         (h (+ 5 (random 7)))
         (c (make-array (list w h) :initial-element nil))
         (anchors
           (list (list 0 (1+ (random (- h 2))))
                 (list (1- w) (1+ (random (- h 2))))
                 (list (1+ (random (- w 2))) 0)
                 (list (1+ (random (- w 2))) (1- h)))))
    ;; dig a template for the room
    (loop for i below w do
      (loop for j below h do
        (when (and (not (zerop i)) (not (zerop j))
                   (not (= i (1- w))) (not (= j (1- h))))
          (setf (aref c i j) t))))
    (make-<room> :width w :height h
                 :anchors anchors
                 :contents c)))

(defun design-cave-room ()
  (let* ((r (make-lake (+ 5 (random 10)) (+ 5 (random 10))))
         (c (<room>-contents r))
         (w (<room>-width r))
         (h (<room>-height r))
         (as nil))
    ;; seal off edges of room
    (loop for i below w do
      (loop for j below h
            when (or (= i (1- w)) (= j (1- h))
                     (= i 0) (= j 0)) do
            (setf (aref (<room>-contents r) i j) nil)))
    ;; add an anchor to each of the four sides of the cave
    (loop for i below w
          when (aref c i 1) do
            (push (list i 0) as)
            (return))
    (loop for i below w
          when (aref c i (- h 2)) do
            (push (list i (1- h)) as)
            (return))
    (loop for i below h
          when (aref c (- w 2) i) do
            (push (list (1- w) i) as)
            (return))
    (loop for i below h
          when (aref c 1 i) do
            (push (list 0 i) as)
            (return))
    (setf (<room>-anchors r) as)
    ;; warning: low risk of lag
    ;; re-generate cave if no anchors can be made
    (if as r (design-cave-room))))


(defun tile-template (template)
  "Turn a structure template (list of strings) into a 2D list of tiles."
  (loop for line in template
        collect (map 'list
                     (lambda (c)
                       (case c
                         (#\# nil)
                         (#\. t)))
                     line)))

(defun design-plus-room ()
  (let* ((template (list "#########"
                         "###...###"
                         "###.#.###"
                         "#...#...#"
                         "#.#####.#"
                         "#...#...#"
                         "###.#.###"
                         "###...###"
                         "#########"))
         (c (make-array '(9 9)
                        :initial-contents
                        (tile-template template))))
    (make-<room> :width 9 :height 9
                 :anchors '((0 4) (8 4) (4 0) (4 8))
                 :contents c)))

(defun new-room ()
  (case (random 5)
    (0 (design-cave-room))
    (1 (design-plus-room))
    (t (design-rect-room))))


(defun shift-room (r shift)
  (setf (<room>-x r) (first shift))
  (setf (<room>-y r) (second shift))
  (setf (<room>-anchors r)
        (mapcar (lambda (a) (add-coords shift a))
                (<room>-anchors r)))
  r)

(defun room-fits (base a r)
  (destructuring-bind (x y) (sub-coords base a)
    (let ((w (<room>-width r))
          (h (<room>-height r))
          (fits t))
      (loop named outer for i below w do
        (loop for j below h
              for xi = (+ x i)
              for yj = (+ y j)
              when (or (not (in-bounds *map* (list xi yj)))
                       (walkable (get-tile xi yj)))
                do (setf fits nil)
                   (return-from outer)))
      fits)))

(defun anchor-room (base r)
  "Position a room `r' by connecting an anchor point to `base'."
  (let* ((corner nil)                   ; top left corner of `r'
         (anch nil)
         ;; choose an anchor to affix to
         (as (loop for a in (<room>-anchors r)
                   when (and (not anch) (room-fits base a r)) do
                     (setf corner (sub-coords base a))
                     (setf anch (add-coords corner a))
                   else collect a)))
    (when corner
      (setf (<room>-anchors r) as)
      (push anch (<room>-used-anchors r))
      (shift-room r corner)
      anch)))

(defun tunnelp (c1 c2)
  "Return a function that determines whether a point can be part of a
properly enclosed tunnel between points `c1' and `c2'."
  (lambda (c) (or (equalp c c1) (equalp c c2)
                  (every (lambda (d)
                           (and (> (first d) 0) (> (second d) 0)
                                (< (first d) (- *width* 2))
                                (< (second d) (- *height* 2))
                                (case (tag (apply #'get-tile d))
                                  (:wall t)
                                  (:wall-frost t))))
                         (neighbor-coords c)))))

(defun accrue-rooms ()
  "Build up a list of adjacent rooms."
  (let ((anchors nil)
        (rooms nil)                     ; rooms that got added
        (count 0))
    ;; create an initial room in the middle of the map
    (let* ((x (truncate (/ *width* 2.0)))
           (y (truncate (/ *height* 2.0)))
           (r (shift-room (new-room) (list x y))))
      (setf anchors (append anchors (<room>-anchors r)))
      (dig-room r)
      (push r rooms))
    ;; accrue rooms
    (loop until (not anchors) do
      (let* ((a (pop anchors))
             (r (new-room))
             (anchor (anchor-room a r)))
        (when anchor
          (incf count)
          (dig-room r)
          (push r rooms)
          (set-tile (first anchor) (second anchor)
                    (if (zerop (random 3))
                        +closed-door+
                        +floor+))
          (setf anchors (append anchors (<room>-anchors r))))))
    ;; return rooms that were generated
    rooms))


(defun dig-tunnel (start end)
  "Try to dig a tunnel from `start' to `end'.  Returns `nil' if no
tunnel could be dug."
  (let* ((path (a-* start end (tunnelp start end)))
         (success (> (length path) 1)))
    (when success
      (loop for (i j) in path do (set-tile i j +floor+))
      (when (>= (length path) 10)
        (set-tile (first start) (second start) +closed-door+)
        (set-tile (first end) (second end) +closed-door+)))
    success))

(defun dig-tunnels (limit coords)
  "Dig a maximum of `limit' tunnels between `coords'."
  (let ((i 0))
    (loop while (and (> (length coords) 1) (< i limit)) do
      (let ((c1 (pop coords))
            (c2 (pop coords)))
        (when (and (> (manhattan-dist c1 c2) 10)
                   (dig-tunnel c1 c2))
          (incf i))))))


(defun random-place-in-room (rooms)
  "Get a random location within a randomly chosen room."
  (let* ((n (random (length rooms)))
         (r (nth n rooms)))
    (list (+ (<room>-x r) (random (<room>-width r)))
          (+ (<room>-y r) (random (<room>-height r))))))

(defun dig-room (r)
  (let ((x (<room>-x r))
        (y (<room>-y r))
        (w (<room>-width r))
        (h (<room>-height r))
        (c (<room>-contents r)))
    (dotimes (i w)
      (dotimes (j h)
        (when (aref c i j)
          (set-tile (+ x i) (+ y j) +floor+))))))



