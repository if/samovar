;;;; samovar.asd

(asdf:defsystem #:samovar
  :description "Describe samovar here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  ;; :serial t
  :depends-on (#:cl-blt
               #:cl-graph
               #:sb-cga
               #:alexandria
               #:priority-queue
               #:cl-speedy-queue
               #:cl-store
               #:black-tie
               #:bobbin)
  :components ((:file "package")
               (:file "util")
               (:file "map" :depends-on ("util"))
               (:file "procgen" :depends-on ("flow" "map"))
               (:file "fov" :depends-on ("monsts" "map"))
               (:file "graphics"
                :depends-on ("util" "map" "monsts" "samovar"))
               (:file "items")
               (:file "levels" :depends-on ("monsts" "map"))
               (:file "flow" :depends-on ("map"))
               (:file "samovar" :depends-on ("items"))
               (:file "ai" :depends-on ("flow"))
               (:file "monsts" :depends-on ("items" "ai"))
               (:file "scenes" :depends-on ("monsts" "items" "graphics" "levels"))
               (:file "main" :depends-on ("procgen" "monsts" "scenes"))))


;; run (asdf:make :samovar/executable)
(defsystem "samovar/executable"
  :build-operation program-op
  :build-pathname "samovar" ;; shell name
  :entry-point "samovar::main" ;; thunk
  :depends-on ("samovar")
  :components ((:file "main") (:file "ai")
               (:file "flow") (:file "fov") (:file "graphics")
               (:file "items") (:file "levels") (:file "map")
               (:file "monsts") (:file "procgen") (:file "samovar")
               (:file "scenes") (:file "util")))
