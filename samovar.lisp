;;;; samovar.lisp
;;;; the samovar mechanism

(in-package #:samovar)

;; if brewing one plain batch of tea leaves, use effect of tea leaves
;; if brewing > 5 batches of tea leaves, make chifir
;; make a negative potion for the following:
;; - rooibos + linden -> petrification
;;   turns monsters to statues, which are just immobile monsters
;;   but you can kill them in case they block your path
;; - oolong + rooibos -> harm
;; - linden + genmaicha -> bottle of mushrooms
;; - oolong + linden -> flashbang/explosive
;; - genmaicha + rooibos -> poison
;; otherwise, make a a 'bitter potion' (tastes bad, but harmless)
(defun samovar-brew (tea-leaves)
  "Brew tea leaves into a potion."
  (let* ((tea (cond
                ((= 1 (length tea-leaves))
                 (brew-tea (effect (car tea-leaves)) (name (car tea-leaves))))
                ((>= (length tea-leaves) 3)
                 (brew-tea :chifir "Chifir"))
                ((sets-equal tea-leaves (list +oolong-tea+ +genmaicha+))
                 (brew-tea :harm "Acidic tea"))
                ((sets-equal tea-leaves (list +oolong-tea+ +sencha+))
                 (brew-tea :swill "Leafy swill"))
                ((sets-equal tea-leaves (list +sencha+ +genmaicha+))
                 (brew-tea :poison "Poisonous tea"))
                (t (brew-tea :nothing "Bitter water")))))
    (set-status (format nil "Brewed [color=cyan]~a[/color]." (name tea)))
    tea))

