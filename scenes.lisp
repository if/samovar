;;;; scenes.lisp

(in-package #:samovar)


(defparameter *scene* :play)

(defparameter *immortal-p* nil
  "Whether the player is immortal.")

(defparameter *has-tick* t
  "Describes whether time has passed in the game world.")

(defun tick-p ()
  "Query whether a tick has occured."
  *has-tick*)

(defun tick ()
  "Tell the game to do another tick."
  (setq *has-tick* t))

(defmacro with-tick (&body body)
  "When there's a new tick, unset it and run the body."
  `(when (tick-p)
     (setq *has-tick* nil)
     ,@body))


(defun item-is-type (it types)
  "Determine whether an item belongs to a certain type."
  (some (curry #'typep it) types))

(defun inventory-select (&optional (types '(t)))
  "Put up an inventory prompt and return the user's selection."
  ;; first, draw the player's inventory
  (setf (blt:color) +white+)
  (setf (blt:background-color) +black+)
  (blt:clear)
  (blt:print 5 1 "--INVENTORY--")
  (flet ((print-item (it i)
           (format nil "(~c) ~a [color=cyan]~a[/color] ~a"
                   (code-char (+ i 97))
                   (if (stackable it)
                       (length (aref *inv* i)) "a")
                   (ch it) (format-item it))))
    (loop for i from 0 to (1- (array-dimension *inv* 0))
          for it = (inventory-peek i)
          with y = 3
          when (and it (item-is-type it types))
            do (blt:print 5 y (print-item it i))
               (incf y)))
  (blt:refresh)
  ;; return the item that the player selects -- may be `nil'
  ;; if the player quits or enters an invalid index
  (let ((k (blt:read)))
    (blt:key-case
     k
     (:escape nil)
     (t (wlet ((i (key-as-index k)))
          (let ((it (inventory-peek i)))
            (and (item-is-type it types) it)))))))

(defun key-as-index (k)
  "Convert a key code to a lower-case alphabetical index,
where a => 0, b => 1, and so on.  Returns `nil' if the key code does
not denote a lower-case alphabetical character."
  (let ((i (- k 4)))
    (when (and (>= i 0) (<= i 26)) i)))


(defun tile-select (start-coord)
  "Select a tile on the map.  Returns the tile's coordinate, or `nil'
if the user quits."
  (let ((coord start-coord)
        (nmonsts (length *visible-monsts*))
        (monst-index 0))
    (flet ((move-coord (dx dy)
             (let ((new-coord (add-coords coord (list dx dy))))
               (when (in-bounds *map* new-coord)
                 (setf coord new-coord))))
           (focus-monst (n)
             (when *visible-monsts*
               (setf coord (monst-coord (nth n *visible-monsts*))))))
      (loop
        (print-map)
        (setf (blt:background-color) +black+)
        (let ((cs (bresenham-line (monst-coord *player*) coord)))
          (setf (blt:color) +yellow+)
          (loop for (i j) in cs do
            (camera-print i j "*")))
        (let ((m (<cell>-monst (apply #'get-cell coord))))
          (if m
              (progn (setf (blt:color) +yellow+)
                     (camera-print (1- (car coord)) (second coord) "[[")
                     (camera-print (1+ (car coord)) (second coord) "]]")
                     (setf (blt:color) +springgreen+)
                     (camera-print (car coord) (second coord) (ch m)))
              (progn (setf (blt:color) +springgreen+)
                     (camera-print (car coord) (second coord) "X"))))
        ;; (format t "cursor is at ~a~%" coord)
        (blt:refresh)
        ;; keyboard navigation
        (blt:key-case
         (blt:read)
         (:enter (return))
         (:escape (setf coord nil) (return))
         (:tab (when (not (zerop nmonsts))
                   (setf monst-index (mod (1+ monst-index) nmonsts))
                   (focus-monst monst-index)))
         (:right (move-coord  1  0))
         (:left  (move-coord -1  0))
         (:up    (move-coord  0 -1))
         (:down  (move-coord  0  1))
         (:6     (move-coord  1  0))
         (:4     (move-coord -1  0))
         (:8     (move-coord  0 -1))
         (:2     (move-coord  0  1))
         (:7     (move-coord -1 -1))
         (:9     (move-coord  1 -1))
         (:1     (move-coord -1  1))
         (:3     (move-coord  1  1))))
      coord)))

(defun look ()
  (wlet ((coord (tile-select (monst-coord *player*))))
    (wlet ((c (and (in-bounds *map* coord)
                   (apply #'get-cell coord))))
      (if (<cell>-visible c)
          (let ((tile (<cell>-tile c)))
            (setf (blt:color) +white+)
            (blt:clear-area 0 0 25 30)
            (blt:print 1 1 (format nil "[color=cyan]You see here:[/color]~%~%~a~%~%~a~%~%~a"
                                   (bobbin:wrap (desc tile) 22)
                                   (if (<cell>-monst c)
                                       (bobbin:wrap
                                        (format nil "~a — ~a Its current health level is ~d, out of a maximum of ~d. It can deal ~d damage, and has a ~d% chance of evading your melee attacks."
                                                (name (<cell>-monst c))
                                                (desc (<cell>-monst c))
                                                (hp (<cell>-monst c))
                                                (max-hp (<cell>-monst c))
                                                (dmg (<cell>-monst c))
                                                (floor (df (<cell>-monst c)) 0.01))
                                        22)
                                       "There are no monsters here.")
                                   (if (<cell>-items c)
                                       (bobbin:wrap
                                        (format nil "Items: ~{~A~#[~:;, ~]~}."
                                                (mapcar #'name (<cell>-items c)))
                                        22)
                                       "There are no items here.")))
            (blt:refresh)
            (blt:read))
          (set-status "You can't see that spot.")))))

(defun throw-item-menu ()
  "Put up a prompt to throw an item."
  ;; note that `wlets' are nested
  (wlet ((src (list (x *player*) (y *player*)))
         (it (inventory-select)))
    (wlet ((dest (tile-select src)))
      (remove-item-ref it)
      (throw-item it src dest)
      (tick))))


(defun quaff-menu ()
  (wlet ((it (inventory-select '(<tea>))))
    (remove-item-ref it)
    (set-status (format nil "You drink the ~a." (name it)))
    (drink-tea it)
    (tick)))

(defun brew-menu ()
  (let* ((inventory (mapcar (lambda (x) (if (listp x) (car x) x))
                            (coerce *inv* 'list)))
         ;; ^ kludge — turn whole inventory into a list
         (selections (select-items-prompt inventory '(<tea-leaves>)))
         (leaves (loop for i from 0 below (array-dimension selections 0)
                       when (aref selections i)
                         collect (inventory-remove i))))
    (when leaves (inventory-add (samovar-brew leaves)) (tick))))

(defun read-menu ()
  (wlet ((it (inventory-select '(<card>))))
    (remove-item-ref it)
    (set-status (format nil "You read the ~a.~%" (name it)))
    (read-card it)
    (tick)))

(defun select-items-prompt (items &optional (types '(t)))
  "Put up a prompt to select from `items'.  Returns an array
containing booleans that say whether each element of the list was
selected."
  (setf (blt:color) +white+)
  (setf (blt:background-color) +black+)
  (blt:clear)
  ;; handle input
  (let* ((valid-indices (loop for item in items
                              collect (item-is-type item types)))
         (valid-items (loop for item in items for i from 0
                            when (nth i valid-indices)
                              collect (list i item)))
         (nitems (length items))
         (selections (make-array nitems :initial-element nil)))
    (loop
      ;; display selected and unselected items
      (blt:print 5 1 "--SELECT ITEMS--")
      (loop for (i it) in valid-items for j from 0 do
        (blt:print 5 (+ j 3)
                   (format nil "~c [[~a]] ~a"
                           (code-char (+ i 97))
                           (if (aref selections i) "x" " ")
                           (name it))))
      (blt:refresh)
      (let ((k (blt:read)))
        (blt:key-case
         k
         (:enter (return))
         (:escape (setf selections (make-array nitems :initial-element nil))
                  (return))
         ;; if user inputs a valid index, toggle corresponding selection
         (t (wlet ((i (key-as-index k)))
              (when (nth i valid-indices)
                (setf (aref selections i)
                      (not (aref selections i)))))))))
    selections))

(defun take-item-menu ()
  "Select items to take from the player's tile."
  ;; list the items on the current tile
  (let* ((items (get-player-cell #'<cell>-items))
         (selections (select-items-prompt items)))
    ;; clear the cell's item stack
    (setf (<cell>-items (get-player-cell)) nil)
    ;; add `take' items to inventory, put `leave' items back onto cell
    (loop for it in items for i from 0
          when (aref selections i) do (inventory-add it)
            else do (place-item it (x *player*) (y *player*)))
    (tick)))

(defun drop-item-menu ()
  "Select an inventory item to drop on the floor"
  (wlet ((it (inventory-select)))
    (drop-item (inventory-find it))
    (tick)))


;; go to the inventory and view the description of an item
(defun inventory-info ()
  (wlet ((it (inventory-select)))
    (set-status (format nil "~a: ~a" (format-item it) (desc it)))))


(defun equip-menu ()
  (wlet ((it (inventory-select '(<weapon> <armor>))))
    (equip it)
    (set-status (format nil "Equipped ~a." (name it)))
    (tick)))

(defun unequip-menu ()
  (wlet ((it (inventory-select '(<weapon> <armor>))))
    (if (equipped it)
        (progn (unequip it)
               (set-status (format nil "Unequipped ~a." (name it)))
               (tick))
        (set-status (format nil "~a was not equipped." (name it))))))

(defun select-dir ()
  "Select a direction and return it as an offset."
  (blt:key-case
   (blt:read)
   (:right '(1  0))
   (:left  '(-1  0))
   (:up    '(0 -1))
   (:down  '(0  1))
   (:6     '(1  0))
   (:4     '(-1  0))
   (:8     '(0 -1))
   (:2     '(0  1))
   (:7     '(-1 -1))
   (:9     '(1 -1))
   (:1     '(-1  1))
   (:3     '(1  1))
   (t nil)))


(defun close-door ()
  (let ((dir (select-dir)))
    (if (not dir)
        (set-status "Never mind.")
        (let* ((c (add-coords (monst-coord *player*) dir))
               (i (first c))
               (j (second c))
               (tl (get-tile i j)))
          (if (not (eq :open-door (tag tl)))
              (set-status "Not an open door.")
              (progn (set-tile i j +closed-door+)
                     (tick)))))))

(defun open-door ()
  (let ((dir (select-dir)))
    (if (not dir)
        (set-status "Never mind.")
        (let* ((c (add-coords (monst-coord *player*) dir))
               (i (first c))
               (j (second c))
               (tl (get-tile i j)))
          (if (not (eq :closed-door (tag tl)))
              (set-status "Not a closed door.")
              (progn (set-tile i j +open-door+)
                     (tick)))))))

(defun jam-door ()
  (let ((dir (select-dir)))
    (if (not dir)
        (set-status "Never mind.")
        (let* ((c (add-coords (monst-coord *player*) dir))
               (i (first c))
               (j (second c))
               (tl (get-tile i j)))
          (if (not (or (eq :open-door (tag tl))
                       (eq :closed-door (tag tl))))
              (set-status "Can't jam that.")
              (progn (set-tile i j +jammed-door+)
                     (set-status "Jammed door.")
                     (wlet ((m (get-cell i j #'<cell>-monst)))
                       (set-status (format nil "The ~a is stuck in the door!~%"
                                           (name m))))
                     (tick)))))))

(defun kick-door ()
  (let ((dir (select-dir)))
    (if (not dir)
        (set-status "Never mind.")
        (let* ((c (add-coords (monst-coord *player*) dir))
               (i (first c))
               (j (second c))
               (tl (get-tile i j)))
          (if (not (or (eq :jammed-door (tag tl))
                       (eq :closed-door (tag tl))
                       (eq :open-door (tag tl))))
              (set-status "Can't kick that.")
              (progn
                (if (< (random 20) 3)
                    (progn (set-tile i j +broken-door+)
                           (set-status "Broke door."))
                    (set-status "Ouch!"))
                (tick)))))))


(defun collect-garbage ()
  "Remove unused objects, like dead monsters."
  (setq *monsts* (filter (lambda (m)
                           (if (eq (state m) :dead)
                               (progn
                                 (cleanup-monst m)
                                 nil)
                               t))
                         *monsts*)))

(defun update-monsts ()
  "Update `*monsts*', removing any whose state turns to `:dead',
and also spawning new monsters if the level is too empty."
  ;; update monsters
  (loop for m in *monsts* do (update-monst m))

  ;; spawn new monsters if there aren't enough
  (let* ((monst-spec (getf (nth *level* *level-recipes*) :monsts))
         (m (first monst-spec))
         (monst-types (second monst-spec))
         (n (length *monsts*))
         (ntypes (length monst-types)))
    (when (<= n (* 0.5 m))
      (loop repeat (* 0.5 n)
            for coord = (find-walkable-place)
            unless (get-cell (first coord) (second coord) #'<cell>-visible)
              do
        (spawn-monst (nth (random ntypes) monst-types)
                     :coord coord
                     :state :sleep)))))




(defun up-level ()
  (if (zerop *level*)
      (if (loop for i below (array-total-size *inv*)
                when (typep (aref *inv* i) '<mellifluous-jasmine>) do
                  (return t))
          (progn
            (set-status "Congratulations, you win!")
            (setq *finished* t)
            (setq *scene* :win))
          (set-status "The door appears to be shut."))
      (progn
        (save-cur-level)
        (load-level (decf *level*))
        (apply #'place-monst *player* *downstairs-pos*)))
  (tick))

(defun down-level ()
  (save-cur-level)
  (if (> (incf *level*) *progress*)
      (progn
        (incf *progress*)
        (init-level))
      (load-level *level*))
  (apply #'place-monst *player* *upstairs-pos*)
  (tick))



(defun play-input (k)
  "Input handler for the main 'scene' of the game."
  (blt:key-case
   k
   (:right (move-monst *player*  1  0) (tick))
   (:left  (move-monst *player* -1  0) (tick))
   (:up    (move-monst *player*  0 -1) (tick))
   (:down  (move-monst *player*  0  1) (tick))
   (:6     (move-monst *player*  1  0) (tick))
   (:4     (move-monst *player* -1  0) (tick))
   (:8     (move-monst *player*  0 -1) (tick))
   (:2     (move-monst *player*  0  1) (tick))
   (:7     (move-monst *player* -1 -1) (tick))
   (:9     (move-monst *player*  1 -1) (tick))
   (:1     (move-monst *player* -1  1) (tick))
   (:3     (move-monst *player*  1  1) (tick))
   
   (:i (inventory-info))
   (:d (drop-item-menu))
   (:q (quaff-menu))
   (:e (equip-menu))
   (:u (unequip-menu))
   (:r (read-menu))
   
   (:b (brew-menu))

   (:o (open-door))
   (:c (close-door))
   (:j (jam-door))
   (:k (kick-door))

   (:t (throw-item-menu))
   (:comma (case (length (get-player-cell #'<cell>-items))
             (0 (set-status "There are no items here to pick up."))
             (1 (take-item (x *player*) (y *player*)))
             (t (take-item-menu))))

   (:l (look))
   
   (:slash
    (let ((c (monst-coord *player*)))
      (cond ((equal c *upstairs-pos*) (up-level))
            ((equal c *downstairs-pos*) (down-level))
            (t (set-status "There are no stairs here.")))))

   (:escape (setq *scene* :quit))

   ;; heal player when at rest
   (:period (monst-heal *player*) (tick))))


(defun run-animations ()
  (loop until (animations-empty-p) do
    (run-animation (dequeue-animation))))

(defun draw-game ()
  "Draw the main game scene (map, HUD, status messages)"  
  (print-hud)
  (print-map))


(defun update-game ()
  "Update game entities and pathfinding fields."

  (setq *player-flow-field*
        (make-flow-field (monst-coord *player*)))
  (setq *player-inv-flow-field*
        (make-reverse-flow-field (monst-coord *player*)
                                 *player-flow-field*))
  
  (update-monst *player*)
  (run-animations)
  (update-monsts)
  (run-animations)
  (when (<= (hp *player*) 0)
    (if *immortal-p*
        (progn (set-status "[color=green]All your wounds heal.[/color]")
               (setf (hp *player*) (max-hp *player*)))
        (setq *scene* :death))))


;;;;; TITLE

(defun init-inventory ()
  (fill *inv* nil)
  (let ((it (make-instance '<flaxen-tunic>)))
    (inventory-add it)
    (equip it))
  (let ((it (make-instance '<quarterstaff>)))
    (inventory-add it)
    (equip it)))


(defun init-level ()
  (init-map)

  (apply #'generate-level (nth *level* *level-recipes*))

  ;; place doors and player
  (let* ((c1 (find-walkable-place))
         (c2 (loop for x = (find-walkable-place)
                   while (equalp x c1)
                   finally (return x))))
    (setq *upstairs-pos* c1)
    (apply #'place-monst *player* c1)
    (place-upstairs c1)
    (unless (= *level* (1- *numlevels*))
      (place-downstairs c2))))

(defun init-game ()
  (set-status "Welcome to the dungeon!")
  (setq *levels* (make-array *numlevels* :initial-element nil))
  (setq *level* 0)
  (setq *progress* 0)
  (setq *player* (new-monst '<player>))
  (setq *monsts* nil)
  (setq *visible-monsts* nil)
  (init-inventory)
  (init-level))

(defun draw-title ()
  (setf (blt:background-color) +black+)
  (setf (blt:color) +white+)
  (blt:clear)
  (blt:print 1 1 "SAMOVAR")
  (blt:print 2 3 "* Press [color=cyan]<ENTER>[/color] to play.")
  (blt:print 2 4 "* Press [color=cyan]<l>[/color] to restore a saved game.")
  (blt:print 2 5 "* Press [color=cyan]<ESC>[/color] to leave."))


(defun load-game-menu ()
  "Prompt the user to restore a savefile."
  (blt:clear)
  (let* ((files (directory #P"*.sv"))
         (nfiles (length files))
         (i 0))
    (loop
      ;; clear everything
      (setf (blt:background-color) +black+)
      (setf (blt:color) +white+)
      (blt:print 1 1 "--RESTORE GAME--")    
      ;; print file names
      (loop for f in files for j from 0 do
        (if (= i j)
          (progn (setf (blt:color) +black+)
                 (setf (blt:background-color) +white+))
          (progn (setf (blt:color) +white+)
                 (setf (blt:background-color) +black+)))
        (blt:print 1 (+ j 4) (format nil "* ~a" f)))
      (blt:refresh)
      ;; get input
      (blt:key-case
       (blt:read)
       (:down (setf i (mod (1+ i) nfiles)))
       (:up   (setf i (mod (1- i) nfiles)))
       (:escape (return))
       (:enter
        (handler-case
            (progn (restore-game (nth i files))
                   (setq *scene* :play)
                   (return))
          (t (c)
            (declare (ignore c))
            (setf (blt:background-color) +black+)
            (setf (blt:color) +red+)
            (blt:print 1 2 "Error: corrupted savefile.")
            (blt:refresh))))))))


;;;;; ENDING

(defun draw-congratulations ()
  (setf (blt:background-color) +black+)
  (setf (blt:color) +white+)
  (blt:clear)
  (blt:print 1 1 "Congratulations, you [color=green]WON![/color]")
  (blt:print 1 3 "[color=cyan]<m>[/color] — return to menu.")
  (blt:print 1 4 "[color=cyan]<q>[/color] — quit."))

(defun draw-death ()
  (setf (blt:background-color) +ultramarine+)
  (setf (blt:color) +white+)
  (blt:clear-area 35 10 30 6)
  (blt:print 36 11 "You [color=red]DIED![/color]")
  (blt:print 36 13 "[color=cyan]<m>[/color] — return to menu.")
  (blt:print 36 14 "[color=cyan]<q>[/color] — quit."))

(defun draw-quit ()
  (setf (blt:background-color) +ultramarine+)
  (setf (blt:color) +white+)
  (blt:clear-area 35 10 30 7)
  (blt:print 36 11 "Save game before quitting?")
  (blt:print 36 13 "[color=cyan]<y>[/color] — save game.")
  (blt:print 36 14 "[color=cyan]<n>[/color] — don't save game.")
  (blt:print 36 15 "[color=cyan]<c>[/color] — cancel."))
