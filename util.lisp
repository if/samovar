;;;; util.lisp
;;;; common utilities
(in-package #:samovar)


(defparameter *level* 0)                ; current level
(defparameter *progress* 0)             ; deepest level reached so far
(defparameter *finished* nil)           ; whether player has won

;; coordinates of a circle with radius 3, centered at the origin
(defparameter +circle-3+
  '((4 2) (4 1) (4 0) (4 -1) (4 -2) (3 3) (3 2) (3 1) (3 0) (3 -1) (3 -2) (3 -3)
    (2 4) (2 3) (2 2) (2 1) (2 0) (2 -1) (2 -2) (2 -3) (2 -4) (1 4) (1 3) (1 2)
    (1 1) (1 0) (1 -1) (1 -2) (1 -3) (1 -4) (0 4) (0 3) (0 2) (0 1) (0 0) (0 -1)
    (0 -2) (0 -3) (0 -4) (-1 4) (-1 3) (-1 2) (-1 1) (-1 0) (-1 -1) (-1 -2)
    (-1 -3) (-1 -4) (-2 4) (-2 3) (-2 2) (-2 1) (-2 0) (-2 -1) (-2 -2) (-2 -3)
    (-2 -4) (-3 3) (-3 2) (-3 1) (-3 0) (-3 -1) (-3 -2) (-3 -3) (-4 2) (-4 1)
    (-4 0) (-4 -1) (-4 -2)))

(defun make-circle (r)
  (let ((coords nil))
    (loop for i from (- r) to r do
      (loop for j from (- r) to r
            when (< (+ (* i i) (* j j)) (* r r))
              do (push (list i j) coords)))
    coords))


;; colors
(defparameter       +white+ (blt:gray :value 1.0))
(defparameter       +black+ (blt:gray :value 0.0))
(defparameter  +dark-khaki+ (blt:rgba #xbd #xb7 #x6b))
(defparameter      +sienna+ (blt:rgba #xa0 #x52 #x2d))
(defparameter         +red+ (blt:rgba #xff #x00 #x00))
(defparameter        +pink+ (blt:rgba #xff #xc0 #xcb))
(defparameter     +magenta+ (blt:rgba #xff #x00 #xff))
(defparameter    +cornsilk+ (blt:rgba #xff #xf8 #xdc))
(defparameter   +goldenrod+ (blt:rgba #xb8 #x86 #x0b)) ; actually it's dark goldenrod
(defparameter       +ivory+ (blt:rgba #xff #xff #xf0))
(defparameter        +gold+ (blt:rgba #xff #xd7 #x00))
(defparameter      +yellow+ (blt:rgba #xff #xff #x00))
(defparameter      +orange+ (blt:rgba #xff #xaa #x00))
(defparameter        +pink+ (blt:rgba #xff #xb7 #xc5))
(defparameter        +cyan+ (blt:rgba #x00 #xff #xff)) ; actually it's coral blue #3
(defparameter +ultramarine+ (blt:rgba #x40 #x00 #xff))
(defparameter   +navy-blue+ (blt:rgba #x00 #x00 #x80))
(defparameter   +pale-blue+ (blt:rgba #xb6 #xe4 #xf7))
(defparameter    +sky-blue+ (blt:rgba #x87 #xce #xeb))
(defparameter  +cornflower+ (blt:rgba #x64 #x95 #xed))
(defparameter  +palepurple+ (blt:rgba #x80 #x00 #x80))
(defparameter  +periwinkle+ (blt:rgba #xcc #xcc #xff))
(defparameter +springgreen+ (blt:rgba #x00 #xfa #x9a))
(defparameter  +chartreuse+ (blt:rgba #x7f #xff #x00))
(defparameter   +darkgreen+ (blt:rgba #x28 #x72 #x28))
(defparameter      +orchid+ (blt:rgba #xda #x70 #xd6))
(defparameter   +porcelain+ (blt:rgba #x89 #xba #xff))


(defun clamp (x lower upper)
  "Clamp a number `x' between a `lower' and `upper' bound."
  (max lower (min upper x)))

;; color util
(defun color-channel (chan color)
  "Extract channel `chan' (:a, :r, :g, :b) from `color'"
  (case chan
    (:a (ash (logand color #xff000000) -24))
    (:r (ash (logand color #x00ff0000) -16))
    (:g (ash (logand color #x0000ff00) -8))
    (:b      (logand color #x000000ff))))

(defun color-channels (color &optional (channels '(:r :g :b :a)))
  (mapcar (lambda (c) (color-channel c color)) channels))

(defun with-alpha (c a)
  "Give color `c' an alpha value of `a'."
  (blt:rgba (color-channel :r c) (color-channel :b c)
            (color-channel :g c) a))

(defun rgb2hsv (rgb)
  "Convert a blt:rgba color to HSV, formatted as '(H S V A),
where A is alpha and is left unchanged from the original."
  (destructuring-bind (r g b a)
      (color-channels rgb)
    (let* ((r* (/ r 255.0))
           (g* (/ g 255.0))
           (b* (/ b 255.0))
           (cmax (max r* g* b*))
           (cmin (min r* g* b*))
           (delta (- cmax cmin)))
      (list
       (if (zerop delta) 0.0
           (* (cond ((zerop cmax) cmax)
                    ((= cmax r*) (mod (/ (- g* b*) delta) 6))
                    ((= cmax g*) (+ (/ (- b* r*) delta) 2))
                    ((= cmax b*) (+ (/ (- r* g*) delta) 4))
                    (t 0))
              60))
       (if (zerop cmax) 0 (/ delta cmax))
       cmax
       a))))

(defun hsv2rgb (hsv)
  (destructuring-bind (h s v a) hsv
    (let* ((c (* v s))
           (x (* c (- 1 (abs (1- (mod (/ h 60) 2))))))
           (m (- v c)))
      (destructuring-bind (r* g* b*)
          (cond
            ((and   (>= h 0) (< h 60)) (list c x 0))
            ((and  (>= h 60) (< h 120)) (list x c 0))
            ((and (>= h 120) (< h 180)) (list 0 c x))
            ((and (>= h 180) (< h 240)) (list 0 x c))
            ((and (>= h 240) (< h 300)) (list x 0 c))
            (t (list c 0 x)))           ; (and ((>= h 180) (< h 240)))
        (blt:rgba
         (round (* 255 (+ m r*)))
         (round (* 255 (+ m g*)))
         (round (* 255 (+ m b*)))
         a)))))


;; because hue is measured in degrees, so the closest interpolation
;; may require going "the other way" along the hue circle.
(defun lerp-hues (h1 h2 s)
  "Linearly interpolate two hue values."
  ;; TODO: this is harebrained and I need to come up with a simpler formula
  (if (> (abs (- h1 h2)) 180)
      (let* ((d (- 360 (max h1 h2)))
             (h1* (mod (+ h1 d) 360))
             (h2* (mod (+ h2 d) 360)))
        (mod (- (lerp-hues h1* h2* s) d) 360))
      (+ h1 (* s (- h2 h1)))))

(defun lighten (color dv &optional dh)
  "Alter the lightness of a color."
  (destructuring-bind (h s v a)
      (rgb2hsv color)
    (hsv2rgb (list
              (if (and dh (< s 0.3)) dh h)
              s
              (clamp (* v dv) 0.0 1.0) a))))

(defun blend-lights (l1 l2)
  (destructuring-bind (dl1 h1) l1
    (destructuring-bind (dl2 h2) l2
      (let ((q (sqrt (+ (* dl1 dl1) (* dl2 dl2)))))
        (list q (cond
                  ((and h1 h2) (lerp-hues h1 h2 q))
                  (h1 h1)
                  (h2 h2)
                  (t nil)))))))

(defun color-shift (amt color)
  "Shift `color' by `amt', where -255 <= amt <= 255"
  (flet ((shift-clamp (chan)
           (max 0 (min #xff (+ amt (color-channel chan color))))))
      (let ((a (color-channel :a color))
            (r (shift-clamp :r))
            (g (shift-clamp :g))
            (b (shift-clamp :b)))
        (logior (ash a 24) (ash r 16) (ash g 8) b))))

;; note: (lerp-colors +palepurple+ +cyan+ (exp (* -0.1 n))) looks cool
(defun lerp-colors (c1 c2 x)
  "Linearly interpolate between `c1' and `c2'.  Returns `c1' when
`x=0' and `c2' when `x=1'."
  (let ((r1 (color-channel :r c1))
        (g1 (color-channel :g c1))
        (b1 (color-channel :b c1))
        (r2 (color-channel :r c2))
        (g2 (color-channel :g c2))
        (b2 (color-channel :b c2)))
      (blt:rgba
       (float2channel (+ r1 (* x (- r2 r1))))
       (float2channel (+ g1 (* x (- g2 g1))))
       (float2channel (+ b1 (* x (- b2 b1)))))))

(defun float2channel (f)
  "Convert a signed float into a color channel, truncating
and clipping as necessary."
  (min 255 (abs (truncate f))))


(defmacro bijective-case (predicate &rest cases)
  "Create a bijective case statement, where each branch is also
defined in reverse.  It is undefined behavior for the same value
to occur in multiple branches."
  `(case ,predicate
     ,@(loop for c in cases
             collect c
             collect (list (second c) (first c)))))

;; this isn't a constant either!
(defun filter (pred list)
  "Filter a list so as to remove all elements not satisfying `pred'."
  (loop for x in list when (funcall pred x) collect x))

(defmacro aref2 (arr coord)
  "2-dimensional aref using a coordinate pair (list) as subscripts"
  `(aref ,arr (first ,coord) (second ,coord)))


(defmacro defflags (&rest flags)
  "Define successive bitflag indices, starting from the least
significant bit."
  `(progn
     ,@(loop for f in flags
             for i from 0 collect
             `(defparameter ,f ,(ash 1 i)))))


(defmacro swap (x1 x2)
  "Generate code that swaps `x1' and `x2'.  Values must be setf'able."
  `(let ((tmp ,x1)) ;; TODO: use `gensym' to make this less unhygienic
     (setf ,x1 ,x2)
     (setf ,x2 tmp)))

(defun rand-char (start end)
  (code-char (+ start (random (- end start)))))

(defun pick-random (xs)
  (nth (random (length xs)) xs))

(defmacro wlet (bindings &body body)
  "Bind some variables and execute the body if that variable is
  non-`nil'.  A combination of `let' over `when'."
  ;; TODO: hygiene
  (let ((names (mapcar #'car bindings)))
      `(let (,@bindings)
         (when (and ,@names) ,@body))))

(defmacro wlet* (bindings &body body)
  "Bind some variables and execute the body if that variable is
  non-`nil'.  A combination of `let*' over `when'."
  ;; TODO: hygiene
  (let ((names (mapcar #'car bindings)))
      `(let* (,@bindings)
         (when (and ,@names) ,@body))))


(defun manhattan-dist (c1 c2)
  "Calculate the Manhattan distance between two coordinates."
  (destructuring-bind (x1 y1) c1
    (destructuring-bind (x2 y2) c2
      (+ (abs (- x1 x2)) (abs (- y1 y2))))))

(defun euclidean-dist (c1 c2)
  "Calculate the Euclidean distance between two coordinates.  For
speed, this doesn't take the root."
  (destructuring-bind (x1 y1) c1
    (destructuring-bind (x2 y2) c2
      (+ (* (- x1 x2) (- x1 x2))
         (* (- y1 y2) (- y1 y2))))))

(defun add-coords (c1 c2)
  (destructuring-bind (x1 y1) c1
    (destructuring-bind (x2 y2) c2
      (list (+ x1 x2) (+ y1 y2)))))

(defun sub-coords (c1 c2)
  (destructuring-bind (x1 y1) c1
    (destructuring-bind (x2 y2) c2
      (list (- x1 x2) (- y1 y2)))))

(defun indexed (xs)
  "Associate an index with each element of a list, in the form (i x)."
  (loop for x in xs for i from 0 collect (list i x)))


(defun dx-dy-dir (dx dy)
  "Determine a compass direction from dx and dy,
assuming dx and dy are +1, 0, or -1.  `:0' is used to indicate zero
change."
  (alexandria:switch ((list dx dy) :test 'equal)
    ('(0   0)  :0)
    ('(0   1)  :s)
    ('(1   0)  :e)
    ('(1   1) :se)
    ('(0  -1)  :n)
    ('(-1  0)  :w)
    ('(-1 -1) :nw)
    ('(-1  1) :sw)
    ('(1  -1) :ne)))

(defun dir-dx-dy (dir)
  "Inverse of `dx-dy-dir'."
  (case dir
    (:0  '(0   0))
    (:s  '(0   1))
    (:e  '(1   0))
    (:se '(1   1))
    (:n  '(0  -1))
    (:w  '(-1  0))
    (:nw '(-1 -1))
    (:sw '(-1  1))
    (:ne '(1  -1))))

(defun opposite-dir (dir)
  (bijective-case
   dir (:n :s) (:e :w) (:ne :sw) (:nw :se)))


(defun sets-equal (s1 s2)
  "Determine whether two sets contain the exact same elements."
  (not (set-difference s1 s2)))
